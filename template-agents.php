<?php
/*
Template Name: Agents
*/
?>

<?php get_header(); ?>
	
	<?php lagomaggiore_homeland_advance_search(); //modify function in "includes/lib/custom-functions.php"... ?>

	<!--AGENT LIST-->
	<section class="theme-pages">

		<div class="inside clear">

			<!--LEFT CONTAINER-->			
			<div class="left-container">
				<div class="agent-container">
					<?php
						$args = array( 'orderby' => 'post_count', 'order' => 'DESC', 'role' => 'contributor' );
					   $homeland_agents = get_users($args);

					    foreach ($homeland_agents as $homeland_user) :
					    	global $wpdb;
							$homeland_post_author = $homeland_user->ID;
							
							$homeland_count = $wpdb->get_var( "SELECT COUNT(*) FROM $wpdb->posts WHERE post_author = $homeland_post_author AND post_type IN ('homeland_properties') and post_status = 'publish'" );
					        ?>
					    	<div class="agent-list clear">

					    		<!--AGENT IMAGE and SOCIAL-->
					    		<div class="agent-image">
					    			<div class="grid cs-style-3">	
					    				<figure class="pimage">
					    					<a href="<?php echo esc_url( get_author_posts_url( $homeland_user->ID ) ); ?>"><?php echo get_avatar( $homeland_user->ID, 230 ); ?></a>
					    					<figcaption><a href="<?php echo esc_url( get_author_posts_url( $homeland_user->ID ) ); ?>"><i class="fa fa-link fa-lg"></i></a></figcaption>
					    				</figure>
					    			</div>
					    			<div class="agent-social">
					    				<ul class="clear">
					    					<li><a href="<?php echo $homeland_user->homeland_facebook; ?>" target="_blank"><i class="fa fa-facebook fa-lg"></i></a></li>
					    					<li><a href="<?php echo $homeland_user->homeland_gplus; ?>" target="_blank"><i class="fa fa-google-plus fa-lg"></i></a></li>
					    					<li><a href="<?php echo $homeland_user->homeland_linkedin; ?>" target="_blank"><i class="fa fa-linkedin fa-lg"></i></a></li>
					    					<li><a href="<?php echo $homeland_user->homeland_twitter; ?>" target="_blank"><i class="fa fa-twitter fa-lg"></i></a></li>
					    					<li class="last"><a href="mailto:<?php echo $homeland_user->user_email; ?>" target="_blank"><i class="fa fa-envelope-o fa-lg"></i></a></li>
					    				</ul>
					    			</div>
					    		</div>

					    		<!--AGENT DESCRIPTIONS-->
					    		<div class="agent-desc">
					    			<h4><a href="<?php echo esc_url( get_author_posts_url( $homeland_user->ID ) ); ?>"><?php echo $homeland_user->display_name; ?></a></h4>
					    			<?php echo wpautop( $homeland_user->user_description ); ?>
					    			<label class="more-info">
					    				<span><i class="fa fa-phone"></i><strong><?php esc_attr( _e( 'Tel no:', CODEEX_THEME_NAME ) ); ?></strong> <?php echo $homeland_user->homeland_telno; ?></span>
					    				<span><i class="fa fa-mobile"></i><strong><?php esc_attr( _e( 'Mobile:', CODEEX_THEME_NAME ) ); ?></strong> <?php echo $homeland_user->homeland_mobile; ?></span>
					    				<span><i class="fa fa-print"></i><strong><?php esc_attr( _e( 'Fax:', CODEEX_THEME_NAME ) ); ?></strong> <?php echo $homeland_user->homeland_fax; ?></span>
					    			</label>
						    		<label class="listed">
						    			<i class="fa fa-home fa-lg"></i> <?php esc_attr( _e( 'Listed:', CODEEX_THEME_NAME ) ); ?>
						    			<span><?php echo intval($homeland_count); echo "&nbsp;"; esc_attr( _e( 'Properties', CODEEX_THEME_NAME ) ); ?></span>
						    		</label>
						    		<a href="<?php echo esc_url( get_author_posts_url( $homeland_user->ID ) ); ?>" class="view-profile">
						    			<?php 
						    				$homeland_agent_button = get_option('homeland_agent_button');

						    				if(!empty( $homeland_agent_button )) : echo $homeland_agent_button;
						    				else : esc_url( _e( 'View Profile', CODEEX_THEME_NAME ) ); 
						    				endif;
						    			?>
						    		</a>
					    		</div>
					    	</div>	
					    	<?php
					    endforeach;
					?>
				</div>
			</div>

			<!--SIDEBAR-->	
			<div class="sidebar"><?php get_sidebar(); ?></div>

		</div>


	</section>

<?php get_footer(); ?>