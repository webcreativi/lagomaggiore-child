<?php get_header(); ?>
	
	<?php lagomaggiore_homeland_advance_search(); //modify function in "includes/lib/custom-functions.php"... ?>

	<!--AGENT LIST-->
	<section class="theme-pages">

		<div class="inside clear">

			<!--LEFT CONTAINER-->			
			<div class="left-container">
				<div class="agent-container">

					<div class="agent-list clear">
						<?php $curauth = (isset($_GET['author_name'])) ? get_user_by('slug', $author_name) : get_userdata(intval($author));	?>

			    		<!--AGENT IMAGE and SOCIAL-->
			    		<div class="agent-image">
			    			<?php echo get_avatar( $curauth->ID, 230 ); ?>
			    			<div class="agent-social">
			    				<ul class="clear">
			    					<li><a href="<?php echo $curauth->homeland_facebook; ?>" target="_blank"><i class="fa fa-facebook fa-lg"></i></a></li>
			    					<li><a href="<?php echo $curauth->homeland_gplus; ?>" target="_blank"><i class="fa fa-google-plus fa-lg"></i></a></li>
			    					<li><a href="<?php echo $curauth->homeland_linkedin; ?>" target="_blank"><i class="fa fa-linkedin fa-lg"></i></a></li>
			    					<li><a href="<?php echo $curauth->homeland_twitter; ?>" target="_blank"><i class="fa fa-twitter fa-lg"></i></a></li>
			    					<li class="last"><a href="mailto:<?php echo $curauth->user_email; ?>" target="_blank"><i class="fa fa-envelope-o fa-lg"></i></a></li>
			    				</ul>
			    			</div>
			    		</div>

			    		<!--AGENT DESCRIPTIONS-->
			    		<div class="agent-desc">
			    			<h4><?php echo $curauth->display_name; ?></h4>
			    			<?php echo wpautop( $curauth->user_description ); ?>
			    			<label class="more-info">
			    				<span><i class="fa fa-phone"></i><strong><?php esc_attr( _e( 'Tel no:', CODEEX_THEME_NAME ) ); ?></strong> <?php echo $curauth->homeland_telno; ?></span>
			    				<span><i class="fa fa-mobile"></i><strong><?php esc_attr( _e( 'Mobile:', CODEEX_THEME_NAME ) ); ?></strong> <?php echo $curauth->homeland_mobile; ?></span>
			    				<span><i class="fa fa-print"></i><strong><?php esc_attr( _e( 'Fax:', CODEEX_THEME_NAME ) ); ?></strong> <?php echo $curauth->homeland_fax; ?></span>
			    			</label>
			    		</div>
			    	</div>

			    	<div class="agent-properties">
			    		<?php
				    		global $wpdb;
							$homeland_post_author = $curauth->ID;
							$homeland_count = $wpdb->get_var( "SELECT COUNT(*) FROM $wpdb->posts WHERE post_author = $homeland_post_author AND post_type IN ('homeland_properties') and post_status = 'publish'" );
						?>

						<h3>
							<?php 
								$homeland_agent_header = get_option('homeland_agent_header');

								echo intval($homeland_count) . "&nbsp;"; 
								if(!empty( $homeland_agent_header )) : echo $homeland_agent_header;
								else : esc_attr( _e( 'Listed Properties', CODEEX_THEME_NAME ) ); 
								endif;
							?>
						</h3>

						<?php 
							$args = array( 'post_type' => 'homeland_properties', 'author' => $curauth->ID, 'paged' => $paged );
							$wp_query = new WP_Query( $args );	

							if ( $wp_query->have_posts() ) : ?>
								<div class="grid cs-style-3">
									<ul class="clear">
										<?php
											while ( $wp_query->have_posts() ) : 
												$wp_query->the_post(); 
												get_template_part( 'loop', 'properties' );
									    	endwhile; 
									    ?>
						    		</ul>
						    	</div><?php
					    	endif;
						?>						   
			    	</div>

			    	<?php 
			    		if(esc_attr( get_option('homeland_pnav') )=="Next Previous Link") : 
			    			homeland_next_previous(); //modify function in "functions.php"...
			    		else : homeland_pagination(); //modify function in "functions.php"...
			    		endif; 
			    	?>

				</div>
			</div>

			<!--SIDEBAR-->	
			<div class="sidebar"><?php get_sidebar(); ?></div>

		</div>

	</section>

<?php get_footer(); ?>