<?php
/*
	Template Name: Properties - Left Sidebar
*/
?>

<?php get_header(); ?>
	
	<?php lagomaggiore_homeland_advance_search(); //modify function in "includes/lib/custom-functions.php"... ?>

	<!--PROPERTY LIST-->
	<section class="theme-pages">

		<div class="inside clear">

			<!--LEFT CONTAINER-->			
			<div class="left-container right">		

				<?php 
					//homeland_get_property_category(); //modify function in "functions.php"... 
					lagomaggiore_homeland_property_sort_order(); //modify function in "functions.php"... 
				?>

				<div class="agent-properties property-list clear">
					<?php
						homeland_get_home_pagination(); //modify function in "functions.php"...

						$homeland_album_order = esc_attr( get_option('homeland_album_order') );
						$homeland_album_orderby = esc_attr( get_option('homeland_album_orderby') );
						$homeland_num_properties = esc_attr( get_option('homeland_num_properties') );

						$homeland_filter_order = @$_GET['filter-order'];
						$homeland_filter_sort_by = @$_GET['filter-sort'];

						/*ORDER and SORT*/

						if(!empty($homeland_filter_order)) :
							$args = array( 'post_type' => 'homeland_properties', 'orderby' => $homeland_filter_sort_by, 'order' => $homeland_filter_order, 'posts_per_page' => $homeland_num_properties, 'paged' => $paged );
						else :
							$args = array( 'post_type' => 'homeland_properties', 'orderby' => $homeland_album_orderby, 'order' => $homeland_album_order, 'posts_per_page' => $homeland_num_properties, 'paged' => $paged );
						endif;

						if(!empty($homeland_filter_sort_by)) :
							$args = array( 'post_type' => 'homeland_properties', 'orderby' => $homeland_filter_sort_by, 'order' => $homeland_filter_order, 'posts_per_page' => $homeland_num_properties, 'paged' => $paged, 'meta_key' => 'homeland_price');
						else :
							$args = array( 'post_type' => 'homeland_properties', 'orderby' => $homeland_album_orderby, 'order' => $homeland_album_order, 'posts_per_page' => $homeland_num_properties, 'paged' => $paged );
						endif;

						$wp_query = new WP_Query( $args );	

						if ( $wp_query->have_posts() ) : 
							?>
								<div class="grid cs-style-3">
									<ul class="clear">
										<?php
											while ( $wp_query->have_posts() ) : 
												$wp_query->the_post(); 
												get_template_part( 'loop', 'properties' );
									    	endwhile; 
									    ?>
						    		</ul>
						    	</div>
					    	<?php
					    endif;		
					?>
				</div>
				<?php 
					if(esc_attr( get_option('homeland_pnav') )=="Next Previous Link") : 
						homeland_next_previous(); //modify function in "functions.php"...
					else : homeland_pagination(); //modify function in "functions.php"... 
					endif; 
				?>
			</div>

			<!--SIDEBAR-->	
			<div class="sidebar left"><?php get_sidebar(); ?></div>

		</div>

	</section>

<?php get_footer(); ?>