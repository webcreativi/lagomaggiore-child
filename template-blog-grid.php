<?php
/*
	Template Name: Blog Grid
*/
?>

<?php get_header(); ?>
	
	<?php lagomaggiore_homeland_advance_search(); //modify function in "includes/lib/custom-functions.php"... ?>

	<!--BLOG LIST-->
	<section class="theme-pages">

		<div class="inside clear">

			<!--LEFT CONTAINER-->			
			<div class="left-container">				
				<div class="blog-grid masonry clear">
					<?php
						homeland_get_home_pagination();

						$args = array( 'post_type' => 'post', 'paged' => $paged );		
						$wp_query = new WP_Query( $args );	

						if ($wp_query->have_posts()) : 
							for($homeland_i = 1; $wp_query->have_posts(); $homeland_i++) {
								$wp_query->the_post();			
								$homeland_columns = 3;	
								$homeland_class = 'blist-grid masonry-item ';
								$homeland_class .= ($homeland_i % $homeland_columns == 0) ? 'last' : '';	

								get_template_part( 'loop', 'entry-grid' );								
							}
						else :
							_e( 'You have no blog post yet!', CODEEX_THEME_NAME );
						endif;					
					?>
				</div>
				<?php 
					if(esc_attr( get_option('homeland_pnav') )=="Next Previous Link") : 
						homeland_next_previous(); //modify function in "functions.php"... 
					else : homeland_pagination(); //modify function in "functions.php"...
					endif; 
				?>
			</div>

			<!--SIDEBAR-->	
			<div class="sidebar"><?php get_sidebar(); ?></div>

		</div>

	</section>

<?php get_footer(); ?>