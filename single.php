<?php get_header(); ?>
	
	<?php //lagomaggiore_homeland_advance_search(); //modify function in "includes/lib/custom-functions.php"... ?>

	<!--BLOG LIST-->
	<section class="theme-pages">

		<div class="inside clear">

			<!--LEFT CONTAINER-->			
			<div class="left-container">				
				<div class="blog-list single-blog clear">
					<?php
						if (have_posts()) : 
							if ( post_password_required() ) :
								?><div class="password-protect-content"><?php the_content(); ?></div><?php
							else :
								while (have_posts()) : the_post(); ?>
									<article id="post-<?php the_ID(); ?>" <?php post_class('blist'); ?>>
										<div class="blog-mask">		
											<div class="blog-image">
												<?php 
													$homeland_large_image_url = wp_get_attachment_image_src( get_post_thumbnail_id(), 'homeland_theme_large');

													if ( ( function_exists( 'get_post_format' ) && 'image' == get_post_format( $post->ID ) )  ) :
														if ( has_post_thumbnail() ) : the_post_thumbnail('homeland_theme_large'); endif;
													elseif ( ( function_exists( 'get_post_format' ) && 'video' == get_post_format( $post->ID ) )  ) :
														if(get_post_meta( $post->ID, "homeland_video_host", true ) == "self") : ?>
															<video id="videojs_blog" class="video-js vjs-default-skin" controls preload="none" width="770" height="430" poster="<?php echo $homeland_large_image_url[0]; ?>" data-setup="{}">
																<source src="<?php echo get_post_meta( $post->ID, "homeland_video", true ); ?>" type='video/mp4' />
															    <source src="<?php echo get_post_meta( $post->ID, "homeland_video", true ); ?>" type='video/webm' />
															    <source src="<?php echo get_post_meta( $post->ID, "homeland_video", true ); ?>" type='video/ogg' />
															</video><?php
														else : ?>
															<iframe width="770" height="430" src="<?php echo get_post_meta( $post->ID, "homeland_video", true ); ?>" frameborder="0" allowfullscreen class="sframe"></iframe><?php
														endif;
													elseif ( ( function_exists( 'get_post_format' ) && 'gallery' == get_post_format( $post->ID ) )  ) : ?>
														<div class="blog-flexslider">
															<ul class="slides">
																<?php
																	$homeland_blog_attachment_order = esc_attr( get_option('homeland_blog_attachment_order') );
																	$homeland_blog_attachment_orderby = esc_attr( get_option('homeland_blog_attachment_orderby') );

																	if(get_option('homeland_blog_thumb_slider') !="true") :
																		$args = array( 'post_type' => 'attachment', 'numberposts' => -1, 'order' => $homeland_blog_attachment_order, 'orderby' => $homeland_blog_attachment_orderby, 'post_status' => null, 'post_parent' => $post->ID );
																	else :
																		$homeland_thumb_id = get_post_thumbnail_id( get_the_ID() );
																		$args = array( 'post_type' => 'attachment', 'numberposts' => -1, 'order' => $homeland_blog_attachment_order, 'orderby' => $homeland_blog_attachment_orderby, 'post_status' => null, 'post_parent' => $post->ID, 'exclude' => $homeland_thumb_id );
																	endif;

																	$homeland_attachments = get_posts( $args );
																	if ( $homeland_attachments ) :								
																		foreach ( $homeland_attachments as $homeland_attachment ) :
																			$homeland_attachment_id = $homeland_attachment->ID;
																			$homeland_type = get_post_mime_type( $homeland_attachment->ID ); 

																			switch ( $homeland_type ) {
																				case 'video/mp4': ?>
																					<li>
																						<video id="videojs_gallery" class="video-js vjs-default-skin vjs-big-play-centered" controls preload="none" width="770" height="450" poster="<?php echo get_template_directory_uri(); ?>/img/video-attachment.jpg" data-setup="{}">
																							<source src="<?php echo wp_get_attachment_url( $homeland_attachment->ID ); ?>" type='video/mp4' />
																							<source src="<?php echo wp_get_attachment_url( $homeland_attachment->ID ); ?>" type='video/webm' />
																							<source src="<?php echo wp_get_attachment_url( $homeland_attachment->ID ); ?>" type='video/ogg' />
																						</video>
																					</li><?php
																				break;
																				default: ?>
																					<li><?php echo wp_get_attachment_image( $homeland_attachment->ID, 'homeland_theme_large' ); ?></li><?php
																				break;
																			}
																		endforeach;
																	endif;			
																?>
															</ul>
														</div><?php
													elseif ( ( function_exists( 'get_post_format' ) && 'audio' == get_post_format( $post->ID ) ) ) : 
														echo get_post_meta( $post->ID, 'homeland_audio', TRUE );	
													endif;											
												?>				
											</div>
										</div>
										<div class="blog-list-desc clear">
											<div class="blog-text">
												<?php the_title( '<h4>', '</h4>' ); ?>	
												<div class="blog-icon">
													<?php
														if ( ( function_exists( 'get_post_format' ) && 'image' == get_post_format( $post->ID ) ) ) :
															?><i class="fa fa-picture-o fa-lg"></i><?php		
														elseif ( ( function_exists( 'get_post_format' ) && 'video' == get_post_format( $post->ID ) ) ) :
															?><i class="fa fa-video-camera fa-lg"></i><?php	
														elseif ( ( function_exists( 'get_post_format' ) && 'gallery' == get_post_format( $post->ID ) ) ) :
															?><i class="fa fa-camera-retro fa-lg"></i><?php
														elseif ( ( function_exists( 'get_post_format' ) && 'audio' == get_post_format( $post->ID ) )  ) :
															?><i class="fa fa-music fa-lg"></i><?php		
														endif;
													?>
												</div>
											</div>
											<div class="blog-action">
												<ul class="clear">
													<li><i class="fa fa-calendar"></i><?php the_time(get_option('date_format')); ?></li>
													<li><i class="fa fa-user"></i><?php the_author_meta( 'user_nicename' ); ?></li>
													<li><i class="fa fa-folder-o"></i><?php the_category(', ') ?></li>
													<li><i class="fa fa-comment"></i><a href="<?php the_permalink(); ?>#comments"><?php comments_number('No Comments', '1 Comment', '% Comments'); ?></a></li>				
												</ul>			
											</div>		
										</div>
										<?php the_content(); ?>
									</article>	
									<?php wp_link_pages(); ?>
									<div class="blog-tags"><?php the_tags('','',''); ?></div><?php
									homeland_social_share(); //modify function in "includes/lib/custom-functions.php"...
								endwhile;
							endif;
						endif;	

						if ( post_password_required() ) :
						else : ?>
							<!--AUTHOR-->
							<div class="author-block clear">
								<?php echo get_avatar( get_the_author_meta( 'ID' ), 100 ); ?>
								<h3><?php the_author_meta( 'user_firstname' ); echo "&nbsp;"; the_author_meta( 'user_lastname' ); ?></h3>
								<?php 
				    				$homeland_author_desc = get_the_author_meta( 'description' );
				    				echo wpautop ( $homeland_author_desc ); 
				    			?>
							</div><?php
						endif;
					?>					

					<!--COMMENTS TEMPLATE-->
					<?php comments_template(); //modify function in "comments.php"... ?>

					<!--NEXT/PREV NAV-->
			    	<div class="post-link-blog clear">
						<span class="prev"><?php previous_post_link('%link', '&larr; Previous Post'); ?></span>
						<span class="next"><?php next_post_link('%link', 'Next Post &rarr;'); ?></span>
					</div>

				</div>
			</div>

			<!--SIDEBAR-->	
			<div class="sidebar"><?php get_sidebar(); ?></div>

		</div>

	</section>

<?php get_footer(); ?>