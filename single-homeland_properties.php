<?php get_header(); ?>
	
	<?php // lagomaggiore_homeland_advance_search(); //modify function in "includes/lib/custom-functions.php"... ?>

	<!--BLOG LIST-->
	<section class="theme-pages">

		<div class="inside clear">

			<!--LEFT CONTAINER-->			
			<div class="left-container">				
				<div class="property-list-page single-property clear">
					<?php
						if (have_posts()) : 
							if ( post_password_required() ) :
								?><div class="password-protect-content"><?php the_content(); ?></div><?php
							else :
								while (have_posts()) : the_post(); ?>
									<article id="post-<?php the_ID(); ?>" <?php post_class('plist'); ?>>
										<?php 
											$homeland_property_id = esc_attr( get_post_meta($post->ID, 'homeland_property_id', true) );
											$homeland_attachment_order = esc_attr( get_option('homeland_attachment_order') );
											$homeland_attachment_orderby = esc_attr( get_option('homeland_attachment_orderby') );
											$homeland_thumbnails = get_post_meta( $post->ID, 'homeland_thumbnails', true );
											$homeland_agent_info = esc_attr( get_option('homeland_agent_info') );
											$homeland_other_properties = esc_attr( get_option('homeland_other_properties') );
											$homeland_property_status = get_the_term_list( $post->ID, 'homeland_property_status', ' ', ', ', '' );
											$homeland_price_per = get_post_meta( $post->ID, 'homeland_price_per', true );
											$homeland_price = get_post_meta( $post->ID, 'homeland_price', true );
											$homeland_hide_map = get_option('homeland_hide_map');
											
											if(esc_attr( get_option('homeland_properties_thumb_slider') )!="true") :
												$args = array( 'post_type' => 'attachment', 'numberposts' => -1, 'order' => $homeland_attachment_order, 'orderby' => $homeland_attachment_orderby, 'post_status' => null, 'post_parent' => $post->ID );
											else :
												$homeland_thumb_id = get_post_thumbnail_id( get_the_ID() );
												$args = array( 'post_type' => 'attachment', 'numberposts' => -1, 'order' => $homeland_attachment_order, 'orderby' => $homeland_attachment_orderby, 'post_status' => null, 'post_parent' => $post->ID, 'exclude' => $homeland_thumb_id );
											endif;

											$homeland_attachments = get_posts( $args );									
										?>
										<div class="properties-flexslider">
											<ul class="slides">
												<?php
													if ( $homeland_attachments ) :						
														foreach ( $homeland_attachments as $homeland_attachment ) :
															$homeland_attachment_id = $homeland_attachment->ID;
															$homeland_type = get_post_mime_type( $homeland_attachment->ID ); 
															$homeland_large_image_url = wp_get_attachment_image_src( $homeland_attachment_id, 'full' ); 

															switch ( $homeland_type ) {
																case 'video/mp4': ?>
																	<li>
																		<video id="videojs_gallery" class="video-js vjs-default-skin vjs-big-play-centered" controls preload="none" width="770" height="450" poster="<?php echo get_template_directory_uri(); ?>/img/video-attachment.jpg" data-setup="{}">
																			<source src="<?php echo wp_get_attachment_url( $homeland_attachment->ID ); ?>" type='video/mp4' />
																			<source src="<?php echo wp_get_attachment_url( $homeland_attachment->ID ); ?>" type='video/webm' />
														    				<source src="<?php echo wp_get_attachment_url( $homeland_attachment->ID ); ?>" type='video/ogg' />
																		</video>
																	</li><?php
																break;
																default: ?>
																	<li>
																		<a href="<?php echo esc_url( $homeland_large_image_url[0] ); ?>" rel="gallery" title="<?php _e( 'View Fullscreen', CODEEX_THEME_NAME ); ?>">
																			<?php echo wp_get_attachment_image( $homeland_attachment->ID, 'homeland_theme_large' ); ?>
																		</a>
																	</li><?php
																break;
															}
														endforeach;
													endif;		
												?>
											</ul>	
											<div class="property-page-info clear">
												<div class="property-page-name">
													<?php the_title( '<h3>', '</h3>' ); ?>	
													<label><?php echo esc_attr( get_post_meta($post->ID, 'homeland_address', true) ); ?></label>
												</div>
												<?php
													if(!empty( $homeland_property_status )) : ?>
														<div class="property-page-status">
															<span><?php echo $homeland_property_status; ?></span>
															<?php  echo esc_attr( _e( 'Immobile', CODEEX_THEME_NAME ) ); ?>
														</div><?php
													endif; 
												?>	
												<div class="property-page-price">
													<?php
														if(!empty($homeland_price)) : ?>
															<span class="price">
																<?php 
																	echo esc_attr( get_option('homeland_property_currency') ); 
																	echo number_format ( esc_attr ( $homeland_price ), 0, ".", "." );
																	if(!empty($homeland_price_per)) : echo "/" . $homeland_price_per; endif; 
																?>
															</span><?php
														endif;
														echo get_the_term_list( $post->ID, 'homeland_property_type', ' ', ', ', '' ); 
													?>
												</div>
											</div>								
										</div>

										<?php
											if(empty($homeland_thumbnails)) : ?>
												<div id="carousel-flex" class="properties-flexslider">
													<ul class="slides">
														<?php
															if ( $homeland_attachments ) :						
																foreach ( $homeland_attachments as $homeland_attachment ) : 
																	echo '<li>' . wp_get_attachment_image( $homeland_attachment->ID, 'homeland_property_thumb' ) . '</li>';
																endforeach;
															endif;
														?>		
													</ul>
												</div><?php
											endif;
										?>

										<div class="property-info-agent">

											<span><?php esc_attr( _e( 'Rif: ', CODEEX_THEME_NAME ) ); ?><?php echo $homeland_property_id; ?> </span>
										
											<span>
												<i class="fa fa-home"></i>
												<?php 
													echo esc_attr( get_post_meta($post->ID, 'homeland_area', true) );
													//echo "&nbsp;" .get_post_meta( $post->ID, 'homeland_area_unit', true ); 
												    echo "&nbsp;mq"; 
												
												?>
											</span>
											<span><i class="fa fa-inbox"></i><?php echo esc_attr( get_post_meta($post->ID, 'homeland_bedroom', true) ); ?> <?php esc_attr( _e( 'Locali', CODEEX_THEME_NAME ) ); ?></span>
											<span><i class="fa fa-male"></i><?php echo esc_attr( get_post_meta($post->ID, 'homeland_bathroom', true) ); ?> <?php esc_attr( _e( 'Bagni', CODEEX_THEME_NAME ) ); ?></span>
											<!-- <span><i class="fa fa-car"></i><?php echo esc_attr( get_post_meta($post->ID, 'homeland_garage', true) ); ?> <?php esc_attr( _e( 'Garage', CODEEX_THEME_NAME ) ); ?></span>
											-->
											<?php 
											    $box=get_post_meta($post->ID, 'homeland_garage', true);
												if (!empty($box)) {
											    echo '<span><i class="fa fa-car"></i>'.$box;
											    esc_attr( _e( ' Garage', CODEEX_THEME_NAME ) );	
											    echo '</span>';
											    }
											?>

											<?php 
											    $auto=get_post_meta($post->ID, 'homeland_postoauto', true);
												if (!empty($auto)) {
											    echo '<span><i class="fa fa-car"></i>'.$auto;
											    esc_attr( _e( ' Posti auto', CODEEX_THEME_NAME ) );	
											    echo '</span>';
											    }
											?>
											
											<span><i class="fa fa-calendar"></i><?php esc_attr( _e( 'Anno di costruzione: ', CODEEX_THEME_NAME ) ); ?><?php echo esc_attr( get_post_meta($post->ID, 'homeland_year_built', true) ); ?></span>
											<span><i class="fa fa-sun-o"></i><?php esc_attr( _e( 'Riscaldamento: ', CODEEX_THEME_NAME ) ); ?><?php echo esc_attr( get_post_meta($post->ID, 'homeland_riscaldamento', true) ); ?></span>
											<span><i class="fa fa-star-o"></i><?php esc_attr( _e( 'Classe energetica: ', CODEEX_THEME_NAME ) ); ?><?php echo esc_attr( get_post_meta($post->ID, 'homeland_classeenergetica', true) ); ?></span>
									
											<span><i class="fa fa-coffee"></i><?php esc_attr( _e( 'Cucina: ', CODEEX_THEME_NAME ) ); ?><?php echo esc_attr( get_post_meta($post->ID, 'homeland_cucina', true) ); ?></span>
											<span><i class="fa fa-align-justify"></i><?php esc_attr( _e( 'Balcone: ', CODEEX_THEME_NAME ) ); ?><?php echo esc_attr( get_post_meta($post->ID, 'homeland_balcone', true) ); ?></span>
											<span><i class="fa fa-tree"></i><?php esc_attr( _e( 'Giardino: ', CODEEX_THEME_NAME ) ); ?><?php echo esc_attr( get_post_meta($post->ID, 'homeland_giardino', true) ); ?></span>
											<span><i class="fa fa-unlock-alt"></i><?php esc_attr( _e( 'Cantina: ', CODEEX_THEME_NAME ) ); ?><?php echo esc_attr( get_post_meta($post->ID, 'homeland_cantina', true) ); ?></span>
											<span><i class="fa fa-align-justify"></i><?php esc_attr( _e( 'Terrazzo: ', CODEEX_THEME_NAME ) ); ?><?php echo esc_attr( get_post_meta($post->ID, 'homeland_terrazzo', true) ); ?></span>
											<span><i class="fa fa-money"></i><?php esc_attr( _e( 'Spese condominiali: ', CODEEX_THEME_NAME ) ); ?>€<?php echo esc_attr( get_post_meta($post->ID, 'homeland_spesecondominiali', true) ); ?></span>
									
										</div>
										
										<?php the_content(); ?>

										<?php $lingua=pll_current_language();
										           if ($lingua =="it") {
										                                echo do_shortcode( '[contact-form-7 id="19" title="Richiesta contatto"]' ); ;
										                                }
										            if ($lingua =="en") {
										                                 echo do_shortcode( '[contact-form-7 id="606" title="Request contact"]' );
										                                  }
										?>

										<?php
											if(empty($homeland_hide_map)) : ?>
												<h4>
													<?php
														$homeland_property_map_header = get_option('homeland_property_map_header'); 
														if(!empty( $homeland_property_map_header )) : echo $homeland_property_map_header;
														else : _e( 'Property Map', CODEEX_THEME_NAME );
														endif;
													?>
												</h4>
												<section id="map-property"></section><?php
											endif;
										?>
									</article><?php 
									homeland_social_share(); //modify function in "includes/lib/custom-functions.php"...
								endwhile; 


								if(empty($homeland_agent_info)) :
									?>
										<div class="clear">
											<!--AGENT-->
											<div class="agent-list clear">
									    		<!--AGENT IMAGE and SOCIAL-->
									    		<div class="agent-image">
									    			<div class="grid cs-style-3">	
									    				<figure class="pimage">
									    					<a href="<?php echo esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ); ?>">
									    						<?php echo get_avatar( get_the_author_meta( 'ID' ), 230 ); ?>
									    					</a>
									    					<figcaption><a href="<?php echo esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ); ?>"><i class="fa fa-link fa-lg"></i></a></figcaption>
									    				</figure>
									    			</div>
									    			<div class="agent-social">
									    				<ul class="clear">
									    					<li><a href="<?php the_author_meta( 'homeland_facebook' ); ?>" target="_blank"><i class="fa fa-facebook fa-lg"></i></a></li>
									    					<li><a href="<?php the_author_meta( 'homeland_gplus' ); ?>" target="_blank"><i class="fa fa-google-plus fa-lg"></i></a></li>
									    					<li><a href="<?php the_author_meta( 'homeland_linkedin' ) ?>" target="_blank"><i class="fa fa-linkedin fa-lg"></i></a></li>
									    					<li><a href="<?php the_author_meta( 'homeland_twitter' ); ?>" target="_blank"><i class="fa fa-twitter fa-lg"></i></a></li>
									    					<li class="last"><a href="mailto:<?php the_author_meta( 'user_email' ); ?>" target="_blank"><i class="fa fa-envelope-o fa-lg"></i></a></li>
									    				</ul>
									    			</div>
									    		</div>

									    		<!--AGENT DESCRIPTIONS-->
									    		<div class="agent-desc">
									    			<h4><a href="<?php echo esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ); ?>">
									    				<?php the_author_meta( 'user_firstname' ); echo "&nbsp;"; the_author_meta( 'user_lastname' ); ?></a>
									    			</h4>
									    			<?php 
									    				$homeland_agent_desc = get_the_author_meta( 'description' );
									    				echo wpautop ( $homeland_agent_desc ); 
									    			?>
									    			<label class="more-info">
									    				<span><i class="fa fa-phone"></i><strong><?php esc_attr( _e( 'Tel no:', CODEEX_THEME_NAME ) ); ?></strong> <?php the_author_meta( 'homeland_telno' ); ?></span>
									    				<span><i class="fa fa-mobile"></i><strong><?php esc_attr( _e( 'Mobile:', CODEEX_THEME_NAME ) ); ?></strong> <?php the_author_meta( 'homeland_mobile' ); ?></span>
									    				<span><i class="fa fa-print"></i><strong><?php esc_attr( _e( 'Fax:', CODEEX_THEME_NAME ) ); ?></strong> <?php the_author_meta( 'homeland_fax' ); ?></span>
									    			</label>
										    		<a href="<?php echo esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ); ?>" class="view-profile">
										    			<?php 
										    				$homeland_agent_button = get_option('homeland_agent_button'); 

										    				if(!empty( $homeland_agent_button )) : echo $homeland_agent_button;
										    				else : esc_url( _e( 'View Profile', CODEEX_THEME_NAME ) ); 
										    				endif;
										    			?>
										    		</a>
									    		</div>
									    	</div>	

									    	<!--AGENT FORM-->
									    	<div class="agent-form">
									    		<h4>
									    			<?php 
									    				$homeland_agent_form = get_option('homeland_agent_form'); 

									    				if(!empty( $homeland_agent_form )) : echo $homeland_agent_form;
									    				else : _e( 'Contact', CODEEX_THEME_NAME );
									    				endif;
									    			?>
									    		</h4>
									    		<?php
									    			if ( $_SERVER['REQUEST_METHOD'] == 'POST' ) :
									    				$homeland_property_link = "http://" . $_SERVER['HTTP_HOST']  . $_SERVER['REQUEST_URI'];
									    				$homeland_receiver = get_the_author_meta( 'email' );
									    				
														if(!isset($hasError)) :
															$fname = trim($_POST['fname']);
															$email = trim($_POST['email']);
															if(function_exists('stripslashes')) : $message = stripslashes(trim($_POST['message']));
															else : $message = trim($_POST['message']);
															endif;

															$emailTo = $homeland_receiver; 
															$subject = "Prospect Clients";
															$body = "Name: " . $fname . "\n\n";
															$body .= "Email: " . $email . "\n\n";
															$body .= "Message: " . $message . "\n\n";
															$body .= "Property Link: " . $homeland_property_link . "\n\n";
															$headers = "From: " . $fname . " <" . $email . ">\n";
															$headers .= "Content-Type: text/plain; charset=UTF-8\n";
												        	$headers .= "Content-Transfer-Encoding: 8bit\n";
															
															wp_mail( $emailTo, $subject, $body, $headers );		
															$homeland_emailSent = true;

														endif;
													endif;

													if(isset($homeland_emailSent) && $homeland_emailSent == true) :
														?><label class="sent"><?php _e( 'You have successfully send your message to this agent!', CODEEX_THEME_NAME ); ?></label><?php 
													endif; 
									    		?>
									    		<form id="agent-form" action="<?php the_permalink(); ?>" method="post">
									    			<ul>
									    				<li><input name="fname" type="text" class="required" placeholder="<?php _e( 'Name', CODEEX_THEME_NAME ); ?>" /></li>
									    				<li><input name="email" type="email" class="required email" placeholder="<?php _e( 'Email', CODEEX_THEME_NAME ); ?>" /></li>
									    				<li><textarea name="message" class="required" placeholder="<?php _e( 'Message', CODEEX_THEME_NAME ); ?>" /></textarea></li>
									    				<li><input name="btsend" type="submit" value="Send" /></li>
									    			</ul>	
									    		</form>
									    	</div>
										</div>
									<?php
								endif;


								if(empty($homeland_other_properties)) :
									$homeland_other_properties_header = get_option('homeland_other_properties_header'); 

									echo '<div class="property-list clear">';
										echo '<h4>';
											if(!empty( $homeland_other_properties_header )) : echo $homeland_other_properties_header;
											else : esc_attr( _e( 'Other33 Properties', CODEEX_THEME_NAME ) ); 
											endif;
										echo '</h4>';

										echo '<div class="property-four-cols">';
											echo '<div class="property-four-cols">';
												$homeland_exclude_post = $post->ID;
												$args = array( 'post_type' => 'homeland_properties', 'orderby' => 'rand', 'post__not_in' => array($homeland_exclude_post), 'posts_per_page' => 3 );
												$wp_query = new WP_Query( $args );	

												if ($wp_query->have_posts()) :
													echo '<div class="grid cs-style-3 masonry">';
														echo '<ul class="clear">';
															for($homeland_i = 1; $wp_query->have_posts(); $homeland_i++) {
																$wp_query->the_post();			
																$homeland_columns = 3;	
																$homeland_class = 'property-cols ';
																$homeland_class .= ($homeland_i % $homeland_columns == 0) ? 'last' : '';
																
																get_template_part( 'loop', 'property-4cols' );
															}
														echo '</ul>';
													echo '</div>';
												endif;
												wp_reset_query();	
											echo '</div>';
										echo '</div>';
									echo '</div>';
								endif;

								
								/*COMMENTS TEMPLATE*/
								// comments_template(); //modify function in "comments.php"... 

								?>
								
								<!--NEXT/PREV NAV-->
						    	<div class="post-link-blog clear">
								<span class="prev"><?php previous_post_link('%link',  __( '&larr; Previous Property', CODEEX_THEME_NAME ) ); ?></span>
								<span class="next"><?php next_post_link('%link',  __( 'Next Property &rarr;', CODEEX_THEME_NAME ) ); ?></span>
									
								</div>

<?php
							endif;
						endif;	
					?>	
				</div>
			</div>

			<!--SIDEBAR-->	
			<div class="sidebar"><?php get_sidebar(); ?></div>

		</div>

	</section>

<?php get_footer(); ?>