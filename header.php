<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
<meta name="keywords" content="<?php echo esc_attr( get_option('homeland_meta_keywords') ); ?>" />   
<?php
	$homeland_site_layout = esc_attr( get_option('homeland_site_layout') );
	$homeland_favicon = esc_attr( get_option('homeland_favicon') );
	$homeland_enable_slide_bar = get_option('homeland_enable_slide_bar');

	if(empty( $homeland_site_layout )) : ?><meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" /><?php endif;
?>
<title><?php
  if(is_home()):
   bloginfo('name');
    echo " ";
      else:
    wp_title('');
  endif;
?></title>

<link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?> RSS Feed" href="<?php bloginfo('rss2_url'); ?>" />
<?php 
	if(empty( $homeland_favicon )) : ?>
		<link rel="shortcut icon" href="http://themecss.com/img/favicon.ico" /><?php
	else : ?><link rel="shortcut icon" href="<?php echo esc_attr( get_option('homeland_favicon') ); ?>" /><?php
	endif;
?>

<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<?php wp_head(); ?>
</head>

<!--[if lt IE 9]>
<script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->


<body <?php body_class(); ?>>

	<?php
		if(!empty( $homeland_enable_slide_bar )) : ?>
			<!--SLIDING BAR-->
			<div class="top-toggle">
				<div class="sliding-bar">
					<div class="inside clear">
						<div class="header-widgets">
							<?php
								if ( is_active_sidebar( 'homeland_sliding_bar' ) ) : dynamic_sidebar( 'homeland_sliding_bar' );
								else : _e( 'Please add sliding bar widget here...', CODEEX_THEME_NAME );
								endif;
							?>	
						</div>
					</div>
				</div>
				<a href="#" class="slide-toggle">&nbsp;</a>
			</div><?php
		endif;
	?>
	
	<!--CONTAINER-->
	<div <?php if(esc_attr( get_option('homeland_theme_layout') )=="Boxed") : echo "id='container-boxed'"; else : echo "id='container'"; endif; ?> 
		<?php 
			if(get_option('homeland_sticky_header') == "true") : 
				if(get_option('homeland_theme_header') == "Header 2" ) : 
					echo "class='sticky-header-container'"; 
				else : 
					echo "class='sticky-header-container'"; 
				endif; 
			endif;
		?>>

		<header <?php if(get_option('homeland_sticky_header') == "true") : echo "class='sticky-header'"; endif; ?>>		
			<?php
				if(get_option('homeland_theme_header') == "Header 2") : ?>					
					<div class="inside clear coloreconti">
						<?php 
							homeland_theme_logo(); //modify function in "includes/lib/custom-functions.php"...
							homeland_theme_menu(); //modify function in "includes/lib/custom-functions.php"...
						?>				
					</div><?php	
				elseif(get_option('homeland_theme_header') == "Header 3") : ?>					
					<div class="inside clear coloreconti">
						<?php 
							homeland_theme_logo(); //modify function in "includes/lib/custom-functions.php"...
							homeland_theme_menu(); //modify function in "includes/lib/custom-functions.php"...
						?>				
					</div>
					<section class="header-block header-three">
						<div class="inside clear">
							<?php 
								homeland_social_share_header(); //modify function in "includes/lib/custom-functions.php"...
								homeland_call_info_header(); //modify function in "includes/lib/custom-functions.php"...
							?>								
						</div>
					</section><?php	
				else : ?>
					<section class="header-block">
						<div class="inside clear">
							<?php 
								homeland_social_share_header(); //modify function in "includes/lib/custom-functions.php"...
								homeland_call_info_header(); //modify function in "includes/lib/custom-functions.php"...
							?>								
						</div>
					</section>
					<div class="inside clear coloreconti">
						<?php 
							homeland_theme_logo(); //modify function in "includes/lib/custom-functions.php"... 
							homeland_theme_menu(); //modify function in "includes/lib/custom-functions.php"...
						?>				
					</div><?php	
				endif;
			?>

<!-- google+ Posiziona questo tag all'interno del tag head oppure subito prima della chiusura del tag body. -->
<script src="https://apis.google.com/js/platform.js" async defer>
  {lang: 'it'}
</script>
		</header>

		<!--TITLE-->
		<?php 
			homeland_header_image(); //modify function in "functions.php"...
			homeland_template_page_link(); //modify function in "functions.php"...
			homeland_property_terms(); //modify function in "functions.php"...
		?>