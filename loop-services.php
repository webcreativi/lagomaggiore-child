<?php 
	global $post; 
	$homeland_services_button = get_option('homeland_services_button');
	$homeland_custom_link = get_post_meta( $post->ID, 'homeland_custom_link', true );	
?>

<div id="post-<?php the_ID(); ?>" <?php sanitize_html_class( post_class('services-page-list clear') ); ?>>
	<div class="services-page-icon">
		<i class="fa <?php echo esc_html( get_post_meta( $post->ID, "homeland_icon", true ) ); ?> fa-4x"></i>
	</div>						
	<div class="services-page-desc">
		<?php 
			if(!empty($homeland_custom_link)) : the_title( '<h5><a href="' . $homeland_custom_link . '" target="_blank">', '</a></h5>' );
			else : the_title( '<h5><a href="' . get_permalink() . '">', '</a></h5>' );
			endif;

			the_excerpt(); 

			if(!empty($homeland_custom_link)) : ?><a href="<?php echo $homeland_custom_link; ?>" class="read-more" target="_blank"><?php
			else : ?><a href="<?php the_permalink(); ?>" class="read-more"><?php
			endif;
				if(!empty( $homeland_services_button )) : echo $homeland_services_button;
				else : esc_attr( _e( 'Dettagli', CODEEX_THEME_NAME ) ); 
				endif; 
		?>
		</a>
	</div>
</div>