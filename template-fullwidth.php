<?php
/*
	Template Name: Fullwidth
*/
?>

<?php get_header(); ?>
	
	<?php lagomaggiore_homeland_advance_search(); //modify function in "includes/lib/custom-functions.php"... ?>

	<!--THEME CONTENTS-->
	<section class="theme-pages">

		<div class="inside clear">

			<!--FULLWIDTH-->			
			<div class="theme-fullwidth">
				
				<?php
					if (have_posts()) : 
						while (have_posts()) : the_post(); 
							the_content(); 								
						endwhile; 
					endif;
				?>
					
			</div>

		</div>

	</section>

<?php get_footer(); ?>