<?php
/*
	Template Name: About
*/
?>

<?php get_header(); ?>
	
	<?php lagomaggiore_homeland_advance_search(); //modify function in "includes/lib/custom-functions.php"... ?>

	<!--AGENT LIST-->
	<section class="theme-pages">

		<div class="inside clear">

			<!--LEFT CONTAINER-->			
			<div class="left-container">

				<!--AGENT LIST-->
				<div class="agent-about-list clear">
					<h3>
						<?php 
							$homeland_team_header = get_option('homeland_team_header');

							if(!empty( $homeland_team_header )) : echo $homeland_team_header;
							else : esc_attr( _e( 'Our Team', CODEEX_THEME_NAME ) );
							endif;
						?>
					</h3>
					<?php
						$args = array( 'orderby' => 'post_count', 'order' => 'DESC', 'role' => 'contributor' );
					    $homeland_agents = get_users($args);					   

					    foreach ($homeland_agents as $homeland_user) :
					    	?>
					    		<!--AGENT IMAGE and SOCIAL-->
					    		<div class="agent-image">
					    			<div class="grid cs-style-3">	
					    				<figure class="pimage">
					    					<a href="<?php echo esc_url( get_author_posts_url( $homeland_user->ID ) ); ?>"><?php echo get_avatar( $homeland_user->ID, 230 ); ?></a>
					    					<figcaption><a href="<?php echo esc_url( get_author_posts_url( $homeland_user->ID ) ); ?>"><i class="fa fa-link fa-lg"></i></a></figcaption>
					    				</figure>
					    			</div>
					    			<div class="agent-social">
					    				<ul class="clear">
					    					<li><a href="<?php echo $homeland_user->homeland_facebook; ?>" target="_blank"><i class="fa fa-facebook fa-lg"></i></a></li>
					    					<li><a href="<?php echo $homeland_user->homeland_gplus; ?>" target="_blank"><i class="fa fa-google-plus fa-lg"></i></a></li>
					    					<li><a href="<?php echo $homeland_user->homeland_linkedin; ?>" target="_blank"><i class="fa fa-linkedin fa-lg"></i></a></li>
					    					<li><a href="<?php echo $homeland_user->homeland_twitter; ?>" target="_blank"><i class="fa fa-twitter fa-lg"></i></a></li>
					    					<li class="last"><a href="mailto:<?php echo $homeland_user->user_email; ?>" target="_blank"><i class="fa fa-envelope-o fa-lg"></i></a></li>
					    				</ul>
					    			</div>
					    			<h4><a href="<?php echo esc_url( get_author_posts_url( $homeland_user->ID ) ); ?>"><?php echo $homeland_user->display_name; ?></a></h4>
					    			<h5><?php echo $homeland_user->homeland_designation; ?></h5>
					    		</div>
					    	<?php
					    endforeach;
					?>
				</div>
				
				<?php
					if (have_posts()) : 
						while (have_posts()) : the_post(); 
							the_content(); 								
						endwhile; 
					endif;
				?>
					
			</div>

			<!--SIDEBAR-->	
			<div class="sidebar"><?php get_sidebar(); ?></div>

		</div>

	</section>

<?php get_footer(); ?>