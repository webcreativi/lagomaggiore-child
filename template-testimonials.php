<?php
/*
Template Name: Testimonials
*/
?>

<?php get_header(); ?>
	
	<?php lagomaggiore_homeland_advance_search(); //modify function in "includes/lib/custom-functions.php"... ?>

	<!--AGENT LIST-->
	<section class="theme-pages">

		<div class="inside clear">

			<!--LEFT CONTAINER-->			
			<div class="left-container">
				<div class="services-container">
					<?php
						$args = array( 'post_type' => 'homeland_testimonial', 'paged' => $paged );
						$wp_query = new WP_Query( $args );	

						if ($wp_query->have_posts()) : 
							while ( $wp_query->have_posts() ) : 
								$wp_query->the_post();												
								
								?>
									<div id="post-<?php the_ID(); ?>" <?php sanitize_html_class( post_class('testi-page-list clear') ); ?>>
										<?php 
											if ( has_post_thumbnail() ) : the_post_thumbnail('homeland_theme_thumb'); endif;
										?>	
										<div class="testi-desc">
											<?php the_title( '<h4>', '</h4>' ); ?>
											<h5><?php echo esc_attr( get_post_meta( $post->ID, 'homeland_position', true ) ); ?></h5>
											<?php the_content(); ?>
										</div>
									</div>	
								<?php

							endwhile;	
						endif;
					?>
				</div>
				<?php 
					if(esc_attr( get_option('homeland_pnav') )=="Next Previous Link") : 
						homeland_next_previous(); //modify function in "functions.php"...
					else : homeland_pagination(); //modify function in "functions.php"... 
					endif; 
				?>
			</div>

			<!--SIDEBAR-->	
			<div class="sidebar"><?php get_sidebar(); ?></div>

		</div>

	</section>

<?php get_footer(); ?>