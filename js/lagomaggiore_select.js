  function resetMenu(){
		jQuery('#lagomaggiore_location_prov option, #lagomaggiore_location_localita option').each(function(){
			jQuery(this).attr('disabled', false);
		});
		jQuery('option:first-child', '#lagomaggiore_location_prov, #lagomaggiore_location_localita').attr('selected', true);
	}

jQuery(document).ready(function($) {
	$originalComuni = $('#lagomaggiore_location_localita').html();
	$originalProv = $('#lagomaggiore_location_prov').html();
	$originalRegioni = $('#lagomaggiore_location_regione').html();

	/** cambio nella selezione delle regioni */
	$('#lagomaggiore_location_regione').change(function(){
		// console.log('cambio regioni!');
		// ripristino le select originali
		$('#lagomaggiore_location_localita').html($originalComuni);
		$('#lagomaggiore_location_prov').html($originalProv);
		resetMenu();
		
		// seleziono le province
		var regione = $('#lagomaggiore_location_regione option:selected').val();
		// console.log('regione = ' + regione);
		$('#lagomaggiore_location_prov option, #lagomaggiore_location_localita option').each(function(){
			var myreg = $(this).attr('data-regione');
			// console.log('trovata regione: ' + myreg);
			if(regione != 0 && myreg !== regione && myreg !== ""){
				// console.log('cancello!' + $(this).text());
				$(this).detach();
			}
		});
		// aggiorno i valori visualizzati dal plugin jquery.selectbox
		$('#lagomaggiore_location_prov, #lagomaggiore_location_localita').selectBox('refresh');
	});

	$('#lagomaggiore_location_prov').change(function(){
		// console.log('cambio province!');
		// ripristino la select originale
		$('#lagomaggiore_location_localita').html($originalComuni);
		//resetMenu();

		// seleziono i comuni
		var $selected = $('#lagomaggiore_location_prov option:selected');
		var prov = $selected.val();
		$('#lagomaggiore_location_localita option').each(function(){
			var myprov = $(this).attr('data-prov');
			// console.log('trovata provincia: ' + myprov);
			if(prov != 0 && myprov !== prov && myprov !== ""){
				// console.log('cancello!' + $(this).text());
				$(this).detach();
			}
		});
		// aggiorno i valori visualizzati dal plugin jquery.selectbox
		$('#lagomaggiore_location_localita').selectBox('refresh');
	});

	
	$('#lagomaggiore_location_regione').trigger('change');
	$('#lagomaggiore_location_prov, #lagomaggiore_location_localita').selectBox('refresh');

});