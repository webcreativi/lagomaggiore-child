<?php get_header(); ?>
	
	<?php // lagomaggiore_homeland_advance_search(); //modify function in "includes/lib/custom-functions.php"... ?>

	<!--BLOG LIST-->
	<section class="theme-pages">

		<div class="inside clear">

			<?php
				if(get_option('homeland_archive_layout') == "Blog 3 Columns") : ?>
					<div class="theme-fullwidth">
						<div class="blog-fullwidth masonry clear">
							<?php
								if (have_posts()) : 
									for($homeland_i = 1; have_posts(); $homeland_i++) {
										the_post();			
										$homeland_columns = 3;	
										$homeland_class = 'blist-fullwidth masonry-item ';
										$homeland_class .= ($homeland_i % $homeland_columns == 0) ? 'last' : '';	

										get_template_part( 'loop', 'entry-fullwidth' );								
									}
								else : _e( 'You have no blog post yet!', CODEEX_THEME_NAME );
								endif;					
							?>
						</div>
					</div><?php
				elseif(get_option('homeland_archive_layout') == "Blog 4 Columns") : ?>
					<div class="theme-fullwidth">
						<div class="blog-fullwidth masonry clear">
							<?php
								if (have_posts()) : 
									for($homeland_i = 1; have_posts(); $homeland_i++) {
										the_post();			
										$homeland_columns = 4;	
										$homeland_class = 'blist-fullwidth blog-four-cols masonry-item ';
										$homeland_class .= ($homeland_i % $homeland_columns == 0) ? 'last' : '';	

										get_template_part( 'loop', 'entry-4cols' );								
									}
								else : _e( 'You have no blog post yet!', CODEEX_THEME_NAME );
								endif;					
							?>
						</div>
					</div><?php
				else : ?>
					<!--LEFT CONTAINER-->			
					<div class="left-container">		
						<?php
							if(get_option('homeland_archive_layout') == "Blog Grid") : ?>
								<div class="blog-grid masonry clear">
									<?php
										if (have_posts()) : 
											for($homeland_i = 1; have_posts(); $homeland_i++) {
												the_post();			
												$homeland_columns = 3;	
												$homeland_class = 'blist-grid masonry-item ';
												$homeland_class .= ($homeland_i % $homeland_columns == 0) ? 'last' : '';	

												get_template_part( 'loop', 'entry-grid' );								
											}
										else : _e( 'You have no blog post yet!', CODEEX_THEME_NAME );
										endif;					
									?>
								</div><?php
							else : ?>
								<div class="blog-list clear">
									<?php
										if (have_posts()) : 
											while (have_posts()) : the_post(); 					
												get_template_part( 'loop', 'entry' );								
											endwhile;	
										else : _e( 'You have no blog post yet!', CODEEX_THEME_NAME );
										endif;					
									?>
								</div><?php
							endif;
						?>		
						
						<?php 
							if(esc_attr( get_option('homeland_pnav') )=="Next Previous Link") : 
								homeland_next_previous(); //modify function in "functions.php"...
							else : homeland_pagination(); //modify function in "functions.php"... 
							endif; 
						?>
					</div>

					<!--SIDEBAR-->	
					<div class="sidebar"><?php get_sidebar(); ?></div><?php
				endif;
			?>

		</div>

	</section>

<?php get_footer(); ?>