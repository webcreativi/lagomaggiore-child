<?php get_header(); ?>

	<?php lagomaggiore_homeland_advance_search(); //modify function in "includes/lib/custom-functions.php"... ?>

	<section class="page-block">
		<div class="inside">

			<?php
				$homeland_not_found_large_text = get_option('homeland_not_found_large_text');
				$homeland_not_found_small_text = get_option('homeland_not_found_small_text');
				$homeland_not_found_button = get_option('homeland_not_found_button');
			?>

			<!--CONTENT-->
			<div class="page-not-found">
				<h4>
					<?php
							// if(!empty( $homeland_not_found_large_text )) : echo stripslashes( $homeland_not_found_large_text );
						// else :
							esc_attr( _e( 'Pagina non trovata', CODEEX_THEME_NAME ) ); ?>
							<span><?php // sanitize_title( _e( '404', CODEEX_THEME_NAME ) ); ?></span>
							<?php
						// endif;
					?>
				</h4>
				<h5>
					<?php
						//if(!empty( $homeland_not_found_small_text )) : echo stripslashes( $homeland_not_found_small_text );
						//else :	
						//endif;
							esc_attr( _e( 'La pagina ricercata non è stata trovata.</BR>', CODEEX_THEME_NAME ) );
							esc_attr( _e( 'Se credi che sia un link corrotto, <a href="mailto:info@immobiliarelagomaggiore.it">contattaci</a></BR>', CODEEX_THEME_NAME ) );
							esc_attr( _e( 'Altrimenti prova a rifare una ricerca o ad usare il menu di navigazione.', CODEEX_THEME_NAME ) );
						
					?>
				</h5>
				<!-- <a href="<?php echo esc_url( home_url('/') ); ?>" class="back-home">
					<?php 
						if(!empty( $homeland_not_found_button )) : echo $homeland_not_found_button;
						else : _e( 'Prova', CODEEX_THEME_NAME );
						endif;
					?>

				</a>
			   -->
			</div>

		</div>
	</section>
	
<?php get_footer(); ?>