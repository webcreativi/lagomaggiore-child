<?php
/*
Template Name: Services
*/
?>

<?php get_header(); ?>
	
	<?php lagomaggiore_homeland_advance_search(); //modify function in "includes/lib/custom-functions.php"... ?>

	<!--AGENT LIST-->
	<section class="theme-pages">

		<div class="inside clear">

			<!--LEFT CONTAINER-->			
			<div class="left-container">
				<div class="services-container">
					<?php
						$homeland_services_order = esc_attr( get_option('homeland_services_order') );
						$homeland_services_orderby = esc_attr( get_option('homeland_services_orderby') );
						
						$args = array( 'post_type' => 'homeland_services', 'orderby' => $homeland_services_orderby, 'order' => $homeland_services_order, 'paged' => $paged );
						$wp_query = new WP_Query( $args );	

						if ($wp_query->have_posts()) : 
							while ( $wp_query->have_posts() ) : 
								$wp_query->the_post();												
								get_template_part( 'loop', 'services' );
							endwhile;	
						endif;
					?>
				</div>
				<?php 
					if(esc_attr( get_option('homeland_pnav') )=="Next Previous Link") : 
						homeland_next_previous(); //modify function in "functions.php"...
					else : homeland_pagination(); //modify function in "functions.php"... 
					endif; 
				?>
			</div>

			<!--SIDEBAR-->	
			<div class="sidebar"><?php get_sidebar(); ?></div>

		</div>

	</section>

<?php get_footer(); ?>