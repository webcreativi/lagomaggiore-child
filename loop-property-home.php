<?php 
	global $homeland_class; 
	$homeland_property_status = get_the_term_list ( $post->ID, 'homeland_property_status', ' ', ', ', '' );
	$homeland_price_per = get_post_meta( $post->ID, 'homeland_price_per', true );
	$homeland_price = get_post_meta($post->ID, 'homeland_price', true);
?>

<li id="post-<?php the_ID(); ?>" <?php sanitize_html_class( post_class($homeland_class) ); ?>>
	<div class="property-mask">
		<?php 
			if ( post_password_required() ) :
				?><div class="password-protect-thumb"><i class="fa fa-lock fa-2x"></i></div><?php
			else :
				?>
					<figure class="pimage">
						<a href="<?php the_permalink(); ?>">
							<?php if ( has_post_thumbnail() ) { the_post_thumbnail('homeland_property_medium'); } ?>
						</a>
						<figcaption><a href="<?php the_permalink(); ?>"><i class="fa fa-link fa-lg"></i></a></figcaption>
						<?php
							if(!empty( $homeland_property_status )) : ?><h4><?php echo $homeland_property_status; ?></h4><?php
							endif; 
						?>
						<div class="property-price clear">
							<div class="cat-price">
								<span class="pcategory"><?php echo get_the_term_list( $post->ID, 'homeland_property_type', ' ', ', ', '' ); ?></span>
								<?php
									if( !empty($homeland_price) ) : ?>
										<span class="price">
											<?php 
												echo esc_attr( get_option('homeland_property_currency') ); 
												echo number_format ( esc_attr ( $homeland_price ), 0, ".", "." );
												if(!empty($homeland_price_per)) : echo "/" . $homeland_price_per; endif; 
											?>
										</span><?php
									endif;
								?>
							</div>
							<span class="picon"><i class="fa fa-tag"></i></span>
						</div>
					</figure>
				<?php
			endif;
		?>			
	</div>
	<div class="property-info">
		<span>
			<i class="fa fa-home"></i>
			<?php 
				echo esc_attr( get_post_meta($post->ID, 'homeland_area', true) );
				echo "&nbsp;" . get_post_meta( $post->ID, 'homeland_area_unit', true ); 
			?>
		</span>
		<span><i class="fa fa-inbox"></i><?php echo esc_attr( get_post_meta($post->ID, 'homeland_bedroom', true) ); ?> <?php esc_attr( _e( 'Locali', CODEEX_THEME_NAME ) ); ?></span>
		<span><i class="fa fa-male"></i><?php echo esc_attr( get_post_meta($post->ID, 'homeland_bathroom', true) ); ?> <?php esc_attr( _e( 'Bagni', CODEEX_THEME_NAME ) ); ?></span>
	</div>
	<div class="property-desc">
		<?php 
			the_title( '<h4><a href="' . get_permalink() . '">', '</a></h4>' ); 
			the_excerpt(); 
		?>	
	</div>
</li>