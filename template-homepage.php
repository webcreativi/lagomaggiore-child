<?php
/*
	Template Name: Homepage
*/
?>

<?php get_header(); ?>

	<?php 
		/*Get variables from Theme Options*/

		$homeland_hide_three_cols = get_option('homeland_hide_three_cols');	
		$homeland_hide_two_cols = get_option('homeland_hide_two_cols');	
		$homeland_hide_welcome = get_option('homeland_hide_welcome');	
		$homeland_hide_properties = get_option('homeland_hide_properties');	
		$homeland_hide_services = get_option('homeland_hide_services');	
		$homeland_hide_testimonials = get_option('homeland_hide_testimonials');	

		homeland_slider(); //modify function in "includes/lib/custom-functions.php"...
		homeland_advance_search(); //modify function in "includes/lib/custom-functions.php"...

		if(empty($homeland_hide_services)) : 
			homeland_services_list();
		endif; //modify function in "includes/lib/custom-functions.php"...

		if(empty($homeland_hide_properties)) :
			homeland_property_list();
		endif; //modify function in "includes/lib/custom-functions.php"...
		
		if(empty($homeland_hide_welcome)) :
			homeland_welcome_text();	
		endif; //modify function in "includes/lib/custom-functions.php"...
	
		if(empty($homeland_hide_three_cols)) :
			?>
				<!--3 COLUMNS-->
				<section class="three-columns-block">
					<div class="inside clear">
						<div class="agent-block">
							<h3>
								<span>
									<?php 
										sanitize_title( _e( 'CHI SIAMO', CODEEX_THEME_NAME ) ); 
										
									?>
								</span>
							</h3>
							

						</div>

						<?php 
							// homeland_agent_list(); //modify function in "includes/lib/custom-functions.php"...
							homeland_featured_list(); //modify function in "includes/lib/custom-functions.php"...
							lagomaggiore_homeland_last_list();
							// homeland_blog_latest(); modify function in "includes/lib/custom-functions.php"...
						?>
					</div>
				</section>
			<?php
		endif;
	?>

<?php get_footer(); ?>

