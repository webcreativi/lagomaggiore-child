<?php
/*
	Template Name: Homepage + LAGOMAGGIORE
*/
?>

<?php get_header(); ?>

	<?php 
		/*Get variables from Theme Options*/

		$homeland_hide_three_cols = get_option('homeland_hide_three_cols');	
		$homeland_hide_two_cols = get_option('homeland_hide_two_cols');	
		$homeland_hide_welcome = get_option('homeland_hide_welcome');	
		$homeland_hide_properties = get_option('homeland_hide_properties');	
		$homeland_hide_services = get_option('homeland_hide_services');	
		$homeland_hide_testimonials = get_option('homeland_hide_testimonials');	

		homeland_slider(); //modify function in "includes/lib/custom-functions.php"...
		lagomaggiore_homeland_advance_search(); //modify function in "includes/lib/custom-functions.php"...
		?>
		<section class="theme-pages">

				<div class="inside clear">

					<!--FULLWIDTH-->			
					<div class="theme-fullwidth ">
						<?php
							if (have_posts()) : 
								while (have_posts()) : the_post(); 
									the_content(); 								
								endwhile; 
							endif;
						?>
							
					</div>

				</div>

		</section>

		<?php


		if(empty($homeland_hide_services)) : 
			homeland_services_list();
		endif; //modify function in "includes/lib/custom-functions.php"...

		if(empty($homeland_hide_properties)) :
			homeland_property_list();
		endif; //modify function in "includes/lib/custom-functions.php"...
		
		//if(empty($homeland_hide_welcome)) :
		//	homeland_welcome_text();	
		//endif; //modify function in "includes/lib/custom-functions.php"...
	

		$homeland_welcome_button = get_option('homeland_welcome_button');	
		?>

			<section class="welcome-block">
				<div class="inside">
					<!-- <h2><?php echo stripslashes( esc_attr( get_option('homeland_welcome_header') ) ); ?></h2> -->
					<h2> <?php esc_attr( _e( 'Vuoi vendere o dare in affitto un immobile?', CODEEX_THEME_NAME ) ); ?></h2>
					<label><?php esc_attr( _e( 'Contattaci subito e approfitta dei nostri canali pubblicitari! </br>Effettueremo per te una valutazione gratuita, obiettiva e realistica.', CODEEX_THEME_NAME ) ); ?></label>
					<a href="<?php echo get_option('homeland_welcome_link'); ?>" class="view-property">
						<?php 
							esc_attr( _e( 'Contattaci', CODEEX_THEME_NAME ) ); 
						?>
					</a>
				</div>
			</section>
		<?php
	

		if(empty($homeland_hide_three_cols)) :
			?>
				<!--3 COLUMNS-->
				<section class="three-columns-block">
					<div class="inside clear">
						<div class="agent-block">
							<h3>
								<span>
									<?php 
										sanitize_title( _e( 'Chi siamo', CODEEX_THEME_NAME ) ); ?>
								</span>
							</h3>
							<?php $lingua=pll_current_language();
							?>
								<?php if ($lingua =="it") {
								echo "<p>L’Immobiliare Lago Maggiore di Giovanni Sampaoli, iscritto al ruolo di agente immobiliare con patentino n° 2203, pone la professionalità e la serietà alla base del proprio lavoro.</p><p>E’ in grado di offrire servizi e consulenze riguardanti le problematiche legate al mercato immobiliare, consapevole che la miglior pubblicità è data dall’aver pienamente soddisfatto il cliente.</p>
								     " ;}
								?>
								<?php if ($lingua =="en") {
								echo "<p><strong>The Immobiliare Lago Maggiore of Giovanni Sampaoli</strong>, enrolled to the role of real estate agent with&nbsp;<strong>patents n° 2203</strong>, places the professionality and the seriousness to the base of just the job.</p>
									 <p>It’s in a position to offering to&nbsp;<strong>services and problematic regarding advisings tied real estate</strong>, at the market aware that the better publicity is given totally satisfied having the customer.</p>
								     " ;}
							?>

						</div>
						<?php
							// homeland_agent_list(); //modify function in "includes/lib/custom-functions.php"...
							homeland_featured_list(); //modify function in "includes/lib/custom-functions.php"...
							lagomaggiore_homeland_last_list();
						?>
					</div>
				</section>
			<?php
		endif;
	?>

<?php get_footer(); ?>



