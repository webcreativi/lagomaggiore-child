<?php get_header(); ?>
	
	<?php lagomaggiore_homeland_advance_search(); //modify function in "includes/lib/custom-functions.php"... ?>

	<!--AGENT LIST-->
	<section class="theme-pages">

		<div class="inside clear">

			<!--LEFT CONTAINER-->			
			<div class="left-container">
				<div class="services-container">
					<?php
						if (have_posts()) : 
							while ( have_posts() ) : the_post(); 								
								?>
								<div id="post-<?php the_ID(); ?>" <?php sanitize_html_class( post_class('clear') ); ?>>
									<div class="services-page-icon">
										<i class="fa <?php echo esc_html( get_post_meta( $post->ID, "homeland_icon", true ) ); ?> fa-4x"></i>
									</div>						
									<div class="services-page-desc">
										<?php 
											the_title( '<h5>', '</h5>' ); the_content(); 
										?>
									</div>
								</div>
								<?php							
							endwhile;	
						endif;
					?>
				</div>
			</div>

			<!--SIDEBAR-->	
			<div class="sidebar"><?php get_sidebar(); ?></div>

		</div>

	</section>

<?php get_footer(); ?>