<?php
/*
	Template Name: Blog List - Left Sidebar
*/
?>

<?php get_header(); ?>
	
	<?php lagomaggiore_homeland_advance_search(); //modify function in "includes/lib/custom-functions.php"... ?>

	<!--BLOG LIST-->
	<section class="theme-pages">

		<div class="inside clear">

			<!--LEFT CONTAINER-->			
			<div class="left-container right">				
				<div class="blog-list clear">
					<?php
						homeland_get_home_pagination(); //modify function in "functions.php"...

						$args = array( 'post_type' => 'post', 'paged' => $paged );		
						$wp_query = new WP_Query( $args );	

						if ($wp_query->have_posts()) : 
							while ($wp_query->have_posts()) : $wp_query->the_post(); 					
								get_template_part( 'loop', 'entry' );								
							endwhile;	
						else :
							_e( 'You have no blog post yet!', CODEEX_THEME_NAME );
						endif;					
					?>
				</div>
				<?php 
					if(esc_attr( get_option('homeland_pnav') )=="Next Previous Link") : 
						homeland_next_previous(); //modify function in "functions.php"... 
					else : homeland_pagination(); //modify function in "functions.php"...
					endif; 
				?>
			</div>

			<!--SIDEBAR-->	
			<div class="sidebar left"><?php get_sidebar(); ?></div>

		</div>

	</section>

<?php get_footer(); ?>