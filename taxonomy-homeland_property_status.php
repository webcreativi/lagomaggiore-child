<?php get_header(); ?>
	
	<?php // lagomaggiore_homeland_advance_search(); //modify function in "includes/lib/custom-functions.php"... ?>

	<!--PROPERTY LIST-->
	<section class="theme-pages">

		<div class="inside clear">

			<?php
				$homeland_term = $wp_query->queried_object;
				$homeland_album_order = esc_attr( get_option('homeland_album_order') );
				$homeland_album_orderby = esc_attr( get_option('homeland_album_orderby') );
				$homeland_num_properties = esc_attr( get_option('homeland_num_properties') );

				$homeland_filter_order = @$_GET['filter-order'];
				$homeland_filter_sort_by = @$_GET['filter-sort'];

				if(get_option('homeland_tax_layout') == "2 Columns") : ?>
					<div class="theme-fullwidth">
						<?php lagomaggiore_homeland_property_sort_order(); ?>
						<div class="property-list clear">
							<div class="property-two-cols">
								<?php
									/*ORDER and SORT*/

									if(!empty($homeland_filter_order)) :
										$args = array( 'post_type' => 'homeland_properties', 'taxonomy' => 'homeland_property_status', 'orderby' => $homeland_filter_sort_by, 'order' => $homeland_filter_order, 'posts_per_page' => $homeland_num_properties, 'term' => $homeland_term->slug, 'paged' => $paged );
									else :
										$args = array( 'post_type' => 'homeland_properties', 'taxonomy' => 'homeland_property_status', 'orderby' => $homeland_album_orderby, 'order' => $homeland_album_order, 'posts_per_page' => $homeland_num_properties, 'term' => $homeland_term->slug, 'paged' => $paged );
									endif;

									if(!empty($homeland_filter_sort_by)) :
										$args = array( 'post_type' => 'homeland_properties', 'taxonomy' => 'homeland_property_status', 'orderby' => $homeland_filter_sort_by, 'order' => $homeland_filter_order, 'posts_per_page' => $homeland_num_properties, 'term' => $homeland_term->slug, 'paged' => $paged, 'meta_key' => 'homeland_price' );
									else :
										$args = array( 'post_type' => 'homeland_properties', 'taxonomy' => 'homeland_property_status', 'orderby' => $homeland_album_orderby, 'order' => $homeland_album_order, 'posts_per_page' => $homeland_num_properties, 'term' => $homeland_term->slug, 'paged' => $paged );
									endif;

									$wp_query = new WP_Query( $args );	

									if ($wp_query->have_posts()) : ?>
										<div class="grid cs-style-3 masonry">	
											<ul class="clear">
												<?php
													for($homeland_i = 1; $wp_query->have_posts(); $homeland_i++) {
														$wp_query->the_post();			
														$homeland_columns = 2;	
														$homeland_class = 'property-cols masonry-item ';
														$homeland_class .= ($homeland_i % $homeland_columns == 0) ? 'last' : '';
														
														get_template_part( 'loop', 'property-2cols' );
													}
												?>
											</ul>
										</div><?php	
									endif;
								?>	
							</div>
						</div>
						<?php 
							if(esc_attr( get_option('homeland_pnav') )=="Next Previous Link") : 
								homeland_next_previous(); //modify function in "functions.php"...
							else : homeland_pagination(); //modify function in "functions.php"... 
							endif; 
						?>
					</div><?php
				elseif(get_option('homeland_tax_layout') == "3 Columns") : ?>
					<div class="theme-fullwidth">
						<?php lagomaggiore_homeland_property_sort_order(); ?>
						<div class="property-list clear">
							<div class="property-three-cols">
								<?php
									/*ORDER and SORT*/

									if(!empty($homeland_filter_order)) :
										$args = array( 'post_type' => 'homeland_properties', 'taxonomy' => 'homeland_property_status', 'orderby' => $homeland_filter_sort_by, 'order' => $homeland_filter_order, 'posts_per_page' => $homeland_num_properties, 'term' => $homeland_term->slug, 'paged' => $paged );
									else :
										$args = array( 'post_type' => 'homeland_properties', 'taxonomy' => 'homeland_property_status', 'orderby' => $homeland_album_orderby, 'order' => $homeland_album_order, 'posts_per_page' => $homeland_num_properties, 'term' => $homeland_term->slug, 'paged' => $paged );
									endif;

									if(!empty($homeland_filter_sort_by)) :
										$args = array( 'post_type' => 'homeland_properties', 'taxonomy' => 'homeland_property_status', 'orderby' => $homeland_filter_sort_by, 'order' => $homeland_filter_order, 'posts_per_page' => $homeland_num_properties, 'term' => $homeland_term->slug, 'paged' => $paged, 'meta_key' => 'homeland_price' );
									else :
										$args = array( 'post_type' => 'homeland_properties', 'taxonomy' => 'homeland_property_status', 'orderby' => $homeland_album_orderby, 'order' => $homeland_album_order, 'posts_per_page' => $homeland_num_properties, 'term' => $homeland_term->slug, 'paged' => $paged );
									endif;
									
									$wp_query = new WP_Query( $args );	

									if ($wp_query->have_posts()) : ?>
										<div class="grid cs-style-3 masonry">	
											<ul class="clear">
												<?php
													for($homeland_i = 1; $wp_query->have_posts(); $homeland_i++) {
														$wp_query->the_post();			
														$homeland_columns = 3;	
														$homeland_class = 'property-cols masonry-item ';
														$homeland_class .= ($homeland_i % $homeland_columns == 0) ? 'last' : '';
														
														get_template_part( 'loop', 'property-3cols' );
													}
												?>
											</ul>
										</div><?php	
									endif;
								?>	
							</div>
						</div>
						<?php 
							if(esc_attr( get_option('homeland_pnav') )=="Next Previous Link") : 
								homeland_next_previous(); //modify function in "functions.php"...
							else : homeland_pagination(); //modify function in "functions.php"... 
							endif; 
						?>
					</div><?php
				elseif(get_option('homeland_tax_layout') == "4 Columns") : ?>
					<div class="theme-fullwidth">
						<?php lagomaggiore_homeland_property_sort_order(); ?>
						<div class="property-list clear">
							<div class="property-four-cols">
								<?php
									/*ORDER and SORT*/

									if(!empty($homeland_filter_order)) :
										$args = array( 'post_type' => 'homeland_properties', 'taxonomy' => 'homeland_property_status', 'orderby' => $homeland_filter_sort_by, 'order' => $homeland_filter_order, 'posts_per_page' => $homeland_num_properties, 'term' => $homeland_term->slug, 'paged' => $paged );
									else :
										$args = array( 'post_type' => 'homeland_properties', 'taxonomy' => 'homeland_property_status', 'orderby' => $homeland_album_orderby, 'order' => $homeland_album_order, 'posts_per_page' => $homeland_num_properties, 'term' => $homeland_term->slug, 'paged' => $paged );
									endif;

									if(!empty($homeland_filter_sort_by)) :
										$args = array( 'post_type' => 'homeland_properties', 'taxonomy' => 'homeland_property_status', 'orderby' => $homeland_filter_sort_by, 'order' => $homeland_filter_order, 'posts_per_page' => $homeland_num_properties, 'term' => $homeland_term->slug, 'paged' => $paged, 'meta_key' => 'homeland_price' );
									else :
										$args = array( 'post_type' => 'homeland_properties', 'taxonomy' => 'homeland_property_status', 'orderby' => $homeland_album_orderby, 'order' => $homeland_album_order, 'posts_per_page' => $homeland_num_properties, 'term' => $homeland_term->slug, 'paged' => $paged );
									endif;

									$wp_query = new WP_Query( $args );	

									if ($wp_query->have_posts()) : ?>
										<div class="grid cs-style-3 masonry">	
											<ul class="clear">
												<?php
													for($homeland_i = 1; $wp_query->have_posts(); $homeland_i++) {
														$wp_query->the_post();			
														$homeland_columns = 4;	
														$homeland_class = 'property-cols masonry-item ';
														$homeland_class .= ($homeland_i % $homeland_columns == 0) ? 'last' : '';
														
														get_template_part( 'loop', 'property-4cols' );
													}
												?>
											</ul>
										</div><?php	
									endif;
								?>	
							</div>
						</div>
						<?php 
							if(esc_attr( get_option('homeland_pnav') )=="Next Previous Link") : 
								homeland_next_previous(); //modify function in "functions.php"...
							else : homeland_pagination(); //modify function in "functions.php"... 
							endif; 
						?>
					</div><?php
				else : ?>
					<!--LEFT CONTAINER-->			
					<div class="left-container">	
						<?php lagomaggiore_homeland_property_sort_order(); ?>
						<div class="agent-properties property-list clear">
							<?php
								/*ORDER and SORT*/
								
								if(!empty($homeland_filter_order)) :
									$args = array( 'post_type' => 'homeland_properties', 'taxonomy' => 'homeland_property_status', 'orderby' => $homeland_filter_sort_by, 'order' => $homeland_filter_order, 'posts_per_page' => $homeland_num_properties, 'term' => $homeland_term->slug, 'paged' => $paged );
								else :
									$args = array( 'post_type' => 'homeland_properties', 'taxonomy' => 'homeland_property_status', 'orderby' => $homeland_album_orderby, 'order' => $homeland_album_order, 'posts_per_page' => $homeland_num_properties, 'term' => $homeland_term->slug, 'paged' => $paged );
								endif;

								if(!empty($homeland_filter_sort_by)) :
									$args = array( 'post_type' => 'homeland_properties', 'taxonomy' => 'homeland_property_status', 'orderby' => $homeland_filter_sort_by, 'order' => $homeland_filter_order, 'posts_per_page' => $homeland_num_properties, 'term' => $homeland_term->slug, 'paged' => $paged, 'meta_key' => 'homeland_price' );
								else :
									$args = array( 'post_type' => 'homeland_properties', 'taxonomy' => 'homeland_property_status', 'orderby' => $homeland_album_orderby, 'order' => $homeland_album_order, 'posts_per_page' => $homeland_num_properties, 'term' => $homeland_term->slug, 'paged' => $paged );
								endif;	

								$wp_query = new WP_Query( $args );	

								if ( $wp_query->have_posts() ) : ?>
									<div class="grid cs-style-3">
										<ul class="clear">
											<?php
												while ( $wp_query->have_posts() ) : 
													$wp_query->the_post(); 
													get_template_part( 'loop', 'properties' );
										    	endwhile; 
										    ?>
							    		</ul>
							    	</div><?php
							    endif;		
							?>
						</div>
						<?php 
							if(esc_attr( get_option('homeland_pnav') )=="Next Previous Link") : 
								homeland_next_previous(); //modify function in "functions.php"...
							else : homeland_pagination(); //modify function in "functions.php"... 
							endif; 
						?>
					</div>

					<!--SIDEBAR-->	
					<div class="sidebar"><?php get_sidebar(); ?></div><?php
				endif;
			?>

		</div>

	</section>

<?php get_footer(); ?>