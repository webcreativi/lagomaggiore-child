<?php
/*
	Template Name: Properties 2 Columns
*/
?>

<?php get_header(); ?>
	
	<?php lagomaggiore_homeland_advance_search(); //modify function in "includes/lib/custom-functions.php"... ?>

	<!--PROPERTY LIST-->
	<section class="theme-pages">

		<div class="inside clear">

			<div class="theme-fullwidth">
				<?php 
					//homeland_get_property_category(); //modify function in "functions.php"... 
					lagomaggiore_homeland_property_sort_order(); //modify function in "functions.php"... 
				?>

				<div class="property-list clear">
					<div class="property-two-cols">
						<?php
							homeland_get_home_pagination(); //modify function in "functions.php"...

							$homeland_album_order = esc_attr( get_option('homeland_album_order') );
							$homeland_album_orderby = esc_attr( get_option('homeland_album_orderby') );
							$homeland_num_properties = esc_attr( get_option('homeland_num_properties') );

							$homeland_filter_order = @$_GET['filter-order'];
							$homeland_filter_sort_by = @$_GET['filter-sort'];

							/*ORDER and SORT*/

							if(!empty($homeland_filter_order)) :
								$args = array( 'post_type' => 'homeland_properties', 'orderby' => $homeland_filter_sort_by, 'order' => $homeland_filter_order, 'posts_per_page' => $homeland_num_properties, 'paged' => $paged );
							else :
								$args = array( 'post_type' => 'homeland_properties', 'orderby' => $homeland_album_orderby, 'order' => $homeland_album_order, 'posts_per_page' => $homeland_num_properties, 'paged' => $paged );
							endif;

							if(!empty($homeland_filter_sort_by)) :
								$args = array( 'post_type' => 'homeland_properties', 'orderby' => $homeland_filter_sort_by, 'order' => $homeland_filter_order, 'posts_per_page' => $homeland_num_properties, 'paged' => $paged, 'meta_key' => 'homeland_price');
							else :
								$args = array( 'post_type' => 'homeland_properties', 'orderby' => $homeland_album_orderby, 'order' => $homeland_album_order, 'posts_per_page' => $homeland_num_properties, 'paged' => $paged );
							endif;
							
							$wp_query = new WP_Query( $args );	

							if ($wp_query->have_posts()) : ?>
								<div class="grid cs-style-3 masonry">	
									<ul class="clear">
										<?php
											for($homeland_i = 1; $wp_query->have_posts(); $homeland_i++) {
												$wp_query->the_post();			
												$homeland_columns = 2;	
												$homeland_class = 'property-cols masonry-item ';
												$homeland_class .= ($homeland_i % $homeland_columns == 0) ? 'last' : '';
												
												get_template_part( 'loop', 'property-2cols' );
											}
										?>
									</ul>
								</div><?php	
							endif;
						?>	
					</div>
				</div>
				<?php 
					if(esc_attr( get_option('homeland_pnav') )=="Next Previous Link") : 
						homeland_next_previous(); //modify function in "functions.php"... 
					else : homeland_pagination(); //modify function in "functions.php"...
					endif; 
				?>
			</div>

		</div>

	</section>

<?php get_footer(); ?>