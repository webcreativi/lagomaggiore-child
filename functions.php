<?php

define('LAGOMAGGIORE_DEBUG', false);

/** includo js per il controllo delle select delle province */ 
function lagomaggiore_includi_js(){
	wp_register_script( 'lagomaggiore_select', get_stylesheet_directory_uri() . '/js/lagomaggiore_select.js', array('jquery', 'homeland_selectbox'), '0.5', true );
	wp_enqueue_script( 'lagomaggiore_select' );
}

add_action( 'wp_enqueue_scripts', 'lagomaggiore_includi_js' );

	/******************
	ADVANCE SEARCH
	******************/

// /* SOVRASCRIVO IL CONTENUTO DEL FORM DI RICERCA! */
// 	if(function_exists('homeland_advance_search')){
// 		rename_function('homeland_advance_search', )
// 		override_function('homeland_advance_search', '', 'return override_search();');
// 	}

// 	function override_search(){
// 		return lagomaggiore_homeland_advance_search(); 
// 	}

	function lagomaggiore_homeland_advance_search() {
		$homeland_disable_advance_search = get_option('homeland_disable_advance_search');
		$homeland_hide_advance_search = get_option('homeland_hide_advance_search');	

		if(is_front_page()) : 
			if(empty($homeland_hide_advance_search)) : lagomaggiore_homeland_advance_search_divs(); endif;
		elseif(is_page() || is_single() || is_archive() || is_author() || is_404() ) :
			if(empty($homeland_disable_advance_search)) : lagomaggiore_homeland_advance_search_divs(); endif;
		endif;
	}


	/******************
	ADVANCE SEARCH DIV
	******************/	

	function lagomaggiore_homeland_advance_search_divs() {
		/* if(is_page_template('template-homepage.php') || is_page_template('template-homepage-lagomaggiore.php') || is_page_template('template-homepage2.php') || is_page_template('template-homepage3.php') || is_page_template('template-homepage4.php') || is_page_template('template-homepage-video.php') || is_page_template('template-homepage-revslider.php') || is_page_template('template-homepage-gmap.php')) : $homeland_search_class = "advance-search-block";
		//else : $homeland_search_class = "advance-search-block advance-search-block-page";
		else : $homeland_search_class = "advance-search-block";
		
		endif;
*/
		$homeland_search_class = "advance-search-block";
		?>
			<section class="<?php echo $homeland_search_class; ?>">
				<div class="inside">
					<?php 
						if ( is_active_sidebar( 'homeland_search_type' ) ) : dynamic_sidebar( 'homeland_search_type' );
						else : lagomaggiore_advance_search_form();
						endif;
					?>
				</div>
			</section>
		<?php
	}


	/******************
	ADVANCE SEARCH FORM
	******************/


	function lagomaggiore_advance_search_form() {
		global $homeland_advance_search_page_url;

		$homeland_currency = get_option('homeland_property_currency'); 
		$homeland_location_label = get_option('homeland_location_label');
		$homeland_selectbox_label = get_option('homeland_selectbox_label');
		$homeland_status_label = get_option('homeland_status_label');
		$homeland_selectbox_label = get_option('homeland_selectbox_label');
		$homeland_property_type_label = get_option('homeland_property_type_label');
		$homeland_bed_label = get_option('homeland_bed_label');
		$homeland_bath_label = get_option('homeland_bath_label');
		$homeland_min_price_label = get_option('homeland_min_price_label');
		$homeland_max_price_label = get_option('homeland_max_price_label');
		$homeland_search_button_label = get_option('homeland_search_button_label');
		$homeland_hide_location = get_option('homeland_hide_location');
		$homeland_hide_status = get_option('homeland_hide_status');
		$homeland_hide_property_type = get_option('homeland_hide_property_type');
		$homeland_hide_bed = get_option('homeland_hide_bed');
		$homeland_hide_bath = get_option('homeland_hide_bath');
		$homeland_hide_min_price = get_option('homeland_hide_min_price');
		$homeland_hide_max_price = get_option('homeland_hide_max_price');

		?>
		<form action="<?php echo $homeland_advance_search_page_url; ?>" method="get" id="searchform">
			<ul class="clear">
				<?php
					if(empty( $homeland_hide_location )) :
						$search_term = @$_GET['lagomaggiore_location_localita'];
						$homeland_terms = get_terms('homeland_property_location');
						$lagomaggiore_localita = array();
						$lagomaggiore_province = array();
						$lagomaggiore_regioni = array();

						foreach ($homeland_terms as $homeland_plocation) :
							$lagomaggiore_option = get_option('lagomaggiore_localita_'.$homeland_plocation->term_id);
							$regione_slug = strtolower(str_replace(' ', '-', $lagomaggiore_option['regione']));
							$lagomaggiore_localita[$homeland_plocation->slug] = array($homeland_plocation, $lagomaggiore_option['provincia'], $regione_slug);
							$lagomaggiore_province[$lagomaggiore_option['provincia']] = $regione_slug;
							$lagomaggiore_regioni[$regione_slug] = strtolower($lagomaggiore_option['regione']);
						endforeach;

						ksort($lagomaggiore_localita);
                //codice aggiunto fa francesco

					if(empty( $homeland_hide_pid )) : 
						$homeland_search_term = isset($_GET['pid']); ?>
						<li>
							<label for="pid">
								<?php
									if(!empty( $homeland_pid_label )) : echo $homeland_pid_label;
									else : esc_attr( _e( 'Riferimento', CODEEX_THEME_NAME ) );
									endif;
								?>
							</label>
							<input type="text" name="pid" class="property-id" value="<?php if($homeland_search_term) : echo $_GET['pid']; endif; ?>" />
						</li>
				<?php
					endif;

				?>
						<li>
							<label for="lagomaggiore_location_regione">
								<?php
									$lagomaggiore_regioni = array_unique($lagomaggiore_regioni);
									asort($lagomaggiore_regioni);
									echo esc_attr( _e( 'Regione', CODEEX_THEME_NAME ) );
								?>
							</label>
							<select name="lagomaggiore_location_regione" id="lagomaggiore_location_regione">
								<option value="0">
									<?php
										echo esc_attr( _e( 'Select', CODEEX_THEME_NAME ) );
									?>
								</option>
								<?php
									foreach ($lagomaggiore_regioni as $key => $regione) :
										echo '<option value="'. $key .'"';
										if( isset($_GET['lagomaggiore_location_regione']) && $key == $_GET['lagomaggiore_location_regione'])
											echo ' selected="selected"';
										echo '>'. ucwords($regione) ."</option>". "\n";
									endforeach;
								?>						
							</select>									
						</li>
						<li>
							<label for="lagomaggiore_location_prov">
								<?php
									ksort($lagomaggiore_province);
									echo esc_attr( _e( 'Provincia', CODEEX_THEME_NAME ) );
								?>
							</label>
							<select name="lagomaggiore_location_prov" id="lagomaggiore_location_prov">
								<option value="0" data-regione="">
									<?php
										echo esc_attr( _e( 'Select', CODEEX_THEME_NAME ) );
									?>
								</option>
								<?php
									foreach ($lagomaggiore_province as $provincia => $regione) :
										echo '<option value="' . strtoupper($provincia) .'" data-regione="' . $regione . '"';
										if( isset($_GET['lagomaggiore_location_prov']) && $provincia == $_GET['lagomaggiore_location_prov'])
											echo ' selected="selected"';
										echo '>'. strtoupper($provincia) . '</option>'. "\n";
									endforeach;
								?>						
							</select>									

						</li>
						<li>
							<label for="lagomaggiore_location_localita">
								<?php
									// if(!empty( $homeland_location_label )) : echo $homeland_location_label;
									if(!empty( $homeland_location_label )) : echo esc_attr( _e( 'Location', CODEEX_THEME_NAME ) );
									
									else : esc_attr( _e( 'Location', CODEEX_THEME_NAME ) );
									endif;
								?>
							</label>
							<select name="lagomaggiore_location_localita" id="lagomaggiore_location_localita">
								<option value="0" data-prov="" data-regione="">
									<?php
										/* if(!empty( $homeland_selectbox_label )) : echo $homeland_selectbox_label;
										else : esc_attr( _e( 'Select', CODEEX_THEME_NAME ) );
										endif; */
										esc_attr( _e( 'Select', CODEEX_THEME_NAME ) );
									?>
								</option>
								<?php
									foreach ($lagomaggiore_localita as $localita => $data) :
										$localita_info = $data[0];
										$provincia = strtoupper($data[1]);
										$regione = $data[2];
										echo '<option value="' . $localita .'" data-prov="' . $provincia . '" data-regione="' . $regione . '"';
									  
									  if($search_term == $localita)
									   	echo ' selected="selected"';
									  
									  echo '>' . ucwords($localita_info->name) ."</option>". "\n";
									  
									endforeach;
								?>						
							</select>
						</li>
						
						<?php
					endif;

					if(empty( $homeland_hide_status )) : ?>
						<li>
							<label>
								<?php
									//if(!empty( $homeland_status_label )) : echo $homeland_status_label;
									if(!empty( $homeland_status_label )) : esc_attr( _e( 'Status', CODEEX_THEME_NAME ) );
									else : esc_attr( _e( 'Status', CODEEX_THEME_NAME ) );
									endif;
								?>
							</label>
							<select name="status">
								<option value="" selected="selected">
									<?php
										/* if(!empty( $homeland_selectbox_label )) : echo $homeland_selectbox_label;
										else : esc_attr( _e( 'Select', CODEEX_THEME_NAME ) );
										endif; */
										esc_attr( _e( 'Select', CODEEX_THEME_NAME ) );
									?>
								</option>
								<?php
									$homeland_search_term = @$_GET['status'];
									$homeland_terms = get_terms('homeland_property_status');

									foreach ($homeland_terms as $homeland_pstatus) :
									   if($homeland_search_term == $homeland_pstatus->slug) :
									   	echo '<option value="'. $homeland_pstatus->slug .'" selected="selected">'. $homeland_pstatus->name ."</option>". "\n";
									   else :
									   	echo '<option value="'. $homeland_pstatus->slug .'">'. $homeland_pstatus->name ."</option>". "\n";
									   endif;
									endforeach;
								?>						
							</select>
						</li><?php
					endif;

					if(empty( $homeland_hide_property_type )) : ?>
						<li>
							<label>
								<?php
									//if(!empty( $homeland_property_type_label )) : echo $homeland_property_type_label;
									if(!empty( $homeland_property_type_label )) : esc_attr( _e( 'Property Type', CODEEX_THEME_NAME ) );
									else : esc_attr( _e( 'Property Type', CODEEX_THEME_NAME ) );
									endif;
								?>
							</label>
							<select name="type">
								<option value="" selected="selected">
									<?php
										/* if(!empty( $homeland_selectbox_label )) : echo $homeland_selectbox_label;
										else : esc_attr( _e( 'Select', CODEEX_THEME_NAME ) );
										endif; */
										esc_attr( _e( 'Select', CODEEX_THEME_NAME ) );
									?>
								</option>
								<?php
									$homeland_search_term = @$_GET['type'];
									$homeland_terms = get_terms('homeland_property_type');

									foreach ($homeland_terms as $homeland_ptype) :
										if($homeland_search_term == $homeland_ptype->slug) :
									   	echo '<option value="'. $homeland_ptype->slug .'" selected="selected">'. $homeland_ptype->name ."</option>". "\n";
									   else :
									   	echo '<option value="'. $homeland_ptype->slug .'">'. $homeland_ptype->name ."</option>". "\n";
									   endif;
									endforeach;
								?>
							</select>
						</li><?php
					endif;

					if(empty( $homeland_hide_bed )) : ?>
						<li>
							<label>
								<?php
									//if(!empty( $homeland_bed_label )) : echo $homeland_bed_label;
									if(!empty( $homeland_bed_label )) : esc_attr( _e( 'Bedrooms', CODEEX_THEME_NAME ) );
									
									else : esc_attr( _e( 'Bedrooms', CODEEX_THEME_NAME ) );
									endif;
								?>
							</label>
							<select name="bed" class="small">
								<option value="" selected="selected">
									<?php
										/* if(!empty( $homeland_selectbox_label )) : echo $homeland_selectbox_label;
										else : esc_attr( _e( 'Select', CODEEX_THEME_NAME ) );
										endif; */
										esc_attr( _e( 'Select', CODEEX_THEME_NAME ) );
									?>
								</option>
								<?php
									$homeland_search_term = @$_GET['bed'];
									$homeland_bed_number = get_option('homeland_bed_number');
									$homeland_array = explode(", ", $homeland_bed_number);

									foreach($homeland_array as $homeland_number_option) :
					               if($homeland_search_term == $homeland_number_option) :
					               	echo '<option value="'. $homeland_number_option .'" selected="selected">'. $homeland_number_option .'</option>'. "\n";
					               else :
					                  echo '<option value="'. $homeland_number_option .'">'. $homeland_number_option .'</option>'. "\n";
					               endif;
					            endforeach;
								?>						
							</select>
						</li><?php
					endif;

					if(empty( $homeland_hide_bath )) : ?>
						<li>
							<label>
								<?php
									//if(!empty( $homeland_bath_label )) : echo $homeland_bath_label;
									if(!empty( $homeland_bath_label )) : esc_attr( _e( 'Bathrooms', CODEEX_THEME_NAME ) );									
									else : esc_attr( _e( 'Bathrooms', CODEEX_THEME_NAME ) );
									endif;
								?>
							</label>
							<select name="bath" class="small">
								<option value="" selected="selected">
									<?php
										/* if(!empty( $homeland_selectbox_label )) : echo $homeland_selectbox_label;
										else : esc_attr( _e( 'Select', CODEEX_THEME_NAME ) );
										endif; */
										esc_attr( _e( 'Select', CODEEX_THEME_NAME ) );
									?>
								</option>
								<?php
									$homeland_search_term = @$_GET['bath'];
									$homeland_bath_number = get_option('homeland_bath_number');
									$homeland_array = explode(", ", $homeland_bath_number);

									foreach($homeland_array as $homeland_number_option) :
					               if($homeland_search_term == $homeland_number_option) :
					               	echo '<option value="'. $homeland_number_option .'" selected="selected">'. $homeland_number_option .'</option>'. "\n";
					               else :
					                  echo '<option value="'. $homeland_number_option .'">'. $homeland_number_option .'</option>'. "\n";
					               endif;
					            endforeach;
								?>		
							</select>
						</li><?php
					endif;

					if(empty( $homeland_hide_min_price )) : ?>
						<li>
							<label>
								<?php  
									// if(!empty( $homeland_min_price_label )) : echo $homeland_min_price_label;
									if(!empty( $homeland_min_price_label )) : esc_attr( _e( 'Minimum Price', CODEEX_THEME_NAME ) );
									
									else : esc_attr( _e( 'Minimum Price', CODEEX_THEME_NAME ) );
									endif;
								?>
							</label>
							<select name="min-price" class="small">
								<option value="" selected="selected">
									<?php
										/* if(!empty( $homeland_selectbox_label )) : echo $homeland_selectbox_label;
										else : esc_attr( _e( 'Select', CODEEX_THEME_NAME ) );
										endif; */
										esc_attr( _e( 'Select', CODEEX_THEME_NAME ) );
									?>
								</option>			
								<?php
									$homeland_search_term = @$_GET['min-price'];
									$homeland_min_price_value = get_option('homeland_min_price_value');
									$homeland_array = explode(", ", $homeland_min_price_value);

									foreach($homeland_array as $homeland_number_option) :
					               if($homeland_search_term == $homeland_number_option) :
					               	echo '<option value="'. $homeland_number_option .'" selected="selected">'. $homeland_currency . number_format ($homeland_number_option) .'</option>'. "\n";
					               else :
					                  echo '<option value="'. $homeland_number_option .'">'. $homeland_currency . number_format ($homeland_number_option) .'</option>'. "\n";
					               endif;
					            endforeach;
								?>					
							</select>
						</li><?php
					endif;

					if(empty( $homeland_hide_max_price )) : ?>
						<li>
							<label>
								<?php  
									// if(!empty( $homeland_max_price_label )) : echo $homeland_max_price_label;
									if(!empty( $homeland_max_price_label )) : esc_attr( _e( 'Maximum Price', CODEEX_THEME_NAME ) );
									
									else : esc_attr( _e( 'Maximum Price', CODEEX_THEME_NAME ) );
									endif;
								?>
							</label>
							<select name="max-price" class="small">
								<option value="" selected="selected">
									<?php
										/* if(!empty( $homeland_selectbox_label )) : echo $homeland_selectbox_label;
										else : esc_attr( _e( 'Select', CODEEX_THEME_NAME ) );
										endif; */
										esc_attr( _e( 'Select', CODEEX_THEME_NAME ) );
									?>
								</option>
								<?php
									$homeland_search_term = @$_GET['max-price'];
									$homeland_max_price_value = get_option('homeland_max_price_value');
									$homeland_array = explode(", ", $homeland_max_price_value);

									foreach($homeland_array as $homeland_number_option) :
					               if($homeland_search_term == $homeland_number_option) :
					               	echo '<option value="'. $homeland_number_option .'" selected="selected">'. $homeland_currency . number_format ($homeland_number_option) .'</option>' . "\n";
					               else :
					                  echo '<option value="'. $homeland_number_option .'">'. $homeland_currency . number_format ($homeland_number_option) .'</option>'. "\n";
					               endif;
					            endforeach;
								?>	
							</select>
						</li><?php
					endif;
				?>

				<li class="last">
					<label>&nbsp;</label>
					<!-- <input type="submit" value="<?php if(!empty( $homeland_search_button_label )) : echo $homeland_search_button_label; else : esc_attr( _e( 'Search', CODEEX_THEME_NAME ) ); endif; ?>" />								
					<input type="submit" value="<?php if(!empty( $homeland_search_button_label )) : esc_attr( _e( $homeland_search_button_label, CODEEX_THEME_NAME ) );	 else : esc_attr( _e( 'Search', CODEEX_THEME_NAME ) ); endif; ?>" />	
				   -->
				
					<input type="submit" value="<?php esc_attr( _e( 'Cerca subito', CODEEX_THEME_NAME ) ); ?>" />	

				    
				</li>
			</ul>
		</form><?php
	}


	/**********************************************
	ADVANCE PROPERTY SEARCH
	***********************************************/

	function lagomaggiore_homeland_advance_property_search($homeland_search_args) {
		$homeland_tax_query = array();
		$homeland_meta_query = array();
	
		// print_pre($homeland_search_args);

		// controlli originali
		if( (!empty($_GET['type'])) ) : $homeland_tax_query[] = array( 'taxonomy' => 'homeland_property_type', 'field' => 'slug', 'terms' => $_GET['type'] ); endif;
		if( (!empty($_GET['status'])) ) : $homeland_tax_query[] = array( 'taxonomy' => 'homeland_property_status', 'field' => 'slug', 'terms' => $_GET['status'] ); endif;
		if( (!empty($_GET['bed'])) ) : $homeland_meta_query[] = array( 'key' => 'homeland_bedroom', 'value' => $_GET['bed'], 'type'=> 'NUMERIC', 'compare' => '=' ); endif;
		if( (!empty($_GET['bath'])) ) : $homeland_meta_query[] = array( 'key' => 'homeland_bathroom', 'value' => $_GET['bath'], 'type'=> 'NUMERIC', 'compare' => '=' ); endif;

		if( isset($_GET['min-price']) && isset($_GET['max-price']) ) :
            
      /*BOTH MIN-MAX PRICE*/
     	if($_GET['min-price'] != "" && $_GET['max-price'] != "") :
        	$homeland_min_price = intval($_GET['min-price']);
         $homeland_max_price = intval($_GET['max-price']);
         
				if( $homeland_min_price >= 0 && $homeland_max_price > $homeland_min_price ) :
				  $homeland_meta_query[] = array( 'key' => 'homeland_price', 'value' => array( $homeland_min_price, $homeland_max_price ), 'type' => 'NUMERIC', 'compare' => 'BETWEEN' );
				endif;
      
      /*MINIMUM PRICE*/
      elseif($_GET['min-price'] != "") :
      	$homeland_min_price = $_GET['min-price'];   
      	if( $homeland_min_price > 0 ) : $homeland_meta_query[] = array( 'key' => 'homeland_price', 'value' => $homeland_min_price, 'type' => 'NUMERIC', 'compare' => '>=' ); endif;

      /*MAXIMUM PRICE*/
      elseif($_GET['max-price'] != "") :
        $homeland_max_price = intval($_GET['max-price']);
        if( $homeland_max_price > 0 ) : $homeland_meta_query[] = array( 'key' => 'homeland_price', 'value' => $homeland_max_price, 'type' => 'NUMERIC', 'compare' => '<=' ); endif;
	 		endif;

	  endif;        

	  // controllo per gestire ID
	  $lagomaggiore_property_id = ( isset( $_GET['pid'] ) ) ? strtoupper( $_GET['pid'] ) : '';
		if( (!empty( $lagomaggiore_property_id ) ) ) : $homeland_meta_query[] = array( 'key' => 'homeland_property_id', 'value' => $lagomaggiore_property_id); endif;

	  // controlli aggiunti per gestire regioni e province
		$localita_slug = sanitize_text_field( $_GET['lagomaggiore_location_localita'] );
		$provincia = sanitize_text_field( $_GET['lagomaggiore_location_prov'] );
		$regione = sanitize_text_field( $_GET['lagomaggiore_location_regione'] );

		$localita_ids = lagomaggiore_get_localita_by_prov( $regione, $provincia, $localita_slug );
		if( is_array ($localita_ids ) && !empty( $localita_ids )){
			$homeland_tax_query[] = array( 'taxonomy' => 'homeland_property_location', 'field' => 'id', 'terms' => $localita_ids, 'operator' => 'IN' );
			if( isset( $homeland_search_args['tax_query'] ))
				$homeland_tax_count = count( $homeland_search_args['tax_query'] );
			else
				$homeland_tax_count = 0;
			if( $homeland_tax_count >= 1 ) : 
				$homeland_tax_query['relation'] = 'AND'; 
				array_push( $homeland_search_args['tax_query'], $homeland_tax_query);
			else:
				$homeland_search_args['tax_query'] = $homeland_tax_query;
			endif;
		}
		else
			$homeland_search_args['lagomaggiore_no_query'] = true;

		$homeland_meta_count = count( $homeland_meta_query );
		if( $homeland_meta_count > 1 ) : $homeland_meta_query['relation'] = 'AND'; endif;
		if( $homeland_meta_count > 0 ) : $homeland_search_args['meta_query'] = $homeland_meta_query; endif;

		return $homeland_search_args;
  }

	// aggiungo la mia versione di ricerca	
  add_filter('homeland_advance_search_parameters', 'lagomaggiore_homeland_advance_property_search', 10);

  // rimuovo il filtro sulla query del tema padre
  // lo posso fare solo così perché homeland/functions.php viene caricata DOPO lagomaggiore_child/functions.php
  // e quindi ora quel filtro non è ancora stato applicato
	function lagomaggiore_remove_homeland_search_filter(){
		remove_filter('homeland_advance_search_parameters', 'homeland_advance_property_search');
	}
	add_action( 'after_setup_theme', 'lagomaggiore_remove_homeland_search_filter' );

   /**********************************************
	PROPERTY SORT and ORDER - per il momento non viene utilizzata - vediamo se si nota
	***********************************************/ 


	function lagomaggiore_homeland_property_sort_order() {
		$homeland_path = $_SERVER['REQUEST_URI'];
		?>
			<div class="clear">
				<div class="filter-sort-order">
					<form action="<?php echo $homeland_path; ?>" method="get" class="form-sorting-order">

						<?php
							if(is_page_template('template-property-search.php')) : ?>
								<input type="hidden" name="location" value="<?php echo $_GET['lagomaggiore_location_localita']; ?>">
		                  <input type="hidden" name="status" value="<?php echo $_GET['status']; ?>">
		                  <input type="hidden" name="type" value="<?php echo $_GET['type']; ?>">
		                  <input type="hidden" name="bed" value="<?php echo $_GET['bed']; ?>">
		                  <input type="hidden" name="bath" value="<?php echo $_GET['bath']; ?>">
		                  <input type="hidden" name="min-price" value="<?php echo $_GET['min-price']; ?>">
		                  <input type="hidden" name="max-price" value="<?php echo $_GET['max-price']; ?>"><?php
							endif;
						?>

						<label for="input_order"><?php _e( 'Order', CODEEX_THEME_NAME ); ?></label>
					 	<select name="filter-order" id="input_order">
				         <?php
								$homeland_filter_order = @$_GET['filter-order'];
								//$homeland_array = array('ASC' => __('Crescente', CODEEX_THEME_NAME), 'DESC' => __('Decrescente', CODEEX_THEME_NAME) );
								$homeland_array = array( 'DESC' => __( 'Descending', CODEEX_THEME_NAME ), 'ASC' => __( 'Ascending', CODEEX_THEME_NAME ) );


								foreach($homeland_array as $homeland_order_code => $homeland_order_option) :
	              	if($homeland_filter_order == $homeland_order_code) :
	               		echo '<option value="'. $homeland_order_code .'" selected="selected">'. $homeland_order_option .'</option>';
	               	else :
	                	echo '<option value="'. $homeland_order_code .'">'. $homeland_order_option .'</option>';
	               	endif;
		            endforeach;
							?>		
							?>		
				     	</select>
				     	<label for="input_sort"><?php _e( 'Sort By', CODEEX_THEME_NAME ); ?></label>
					 	<select name="filter-sort" id="input_sort">
							<?php
								$homeland_filter_sort = @$_GET['filter-sort'];
								$homeland_array = array('date' => 'Data', 'title' => 'Nome', 'homeland_price' => 'Prezzo');

								foreach($homeland_array as $homeland_sort_option_value=>$homeland_sort_option) :
				               if($homeland_filter_sort == $homeland_sort_option_value) :
				               	echo '<option value="'. $homeland_sort_option_value .'" selected="selected">'. $homeland_sort_option .'</option>';
				               else :
				                  echo '<option value="'. $homeland_sort_option_value .'">'. $homeland_sort_option .'</option>';
				               endif;
				            endforeach;
							?>		
						</select>                                                                                        
				   </form>	
				</div>
			</div>
		<?php
	}

/***************
	LAST LIST
	***************/

	function lagomaggiore_homeland_last_list() {
		global $post;

		$homeland_property_order = esc_attr( get_option('homeland_property_order') );
		$homeland_property_orderby = esc_attr( get_option('homeland_property_orderby') );
		$homeland_featured_property_limit = esc_attr( get_option('homeland_featured_property_limit') );
		$homeland_featured_property_header = get_option('homeland_featured_property_header');
		?>
			<div class="featured-block">
				<h3>
					<span>
						<?php 
							// if(!empty( $homeland_featured_property_header )) : echo $homeland_featured_property_header;
							// else : sanitize_title( _e( 'Featured Property', CODEEX_THEME_NAME ) ); 
							// endif;
							sanitize_title( _e( 'Novit&agrave;', CODEEX_THEME_NAME ) ); 
						?>
					</span>
				</h3>
				<?php
					// $args = array( 'post_type' => 'homeland_properties', 'orderby' => $homeland_property_orderby, 'order' => $homeland_property_order, 'posts_per_page' => $homeland_featured_property_limit, 'meta_query' => array( array( 'key' => 'homeland_featured', 'value' => 'on', 'compare' => '==' ) ) );		
					$args = array( 'post_type' => 'homeland_properties', 'orderby' => $homeland_property_orderby, 'order' => $homeland_property_order, 'posts_per_page' => $homeland_featured_property_limit  );		
					
					$wp_query = new WP_Query( $args );

					if ($wp_query->have_posts()) : ?>
						<div class="grid cs-style-3">	
							<ul>
								<?php
									while ($wp_query->have_posts()) : $wp_query->the_post(); 
										$homeland_price_per = get_post_meta( $post->ID, 'homeland_price_per', true );
										$homeland_price = get_post_meta($post->ID, 'homeland_price', true );
										?>
										<li id="post-<?php the_ID(); ?>" <?php sanitize_html_class( post_class('featured-list clear') ); ?>>
											<?php
												if ( post_password_required() ) :
													?><div class="password-protect-thumb featured-pass-thumb"><i class="fa fa-lock fa-2x"></i></div><?php
												else :
													if(is_page_template('template-homepage4.php') || is_page_template('template-homepage-video.php' )) : ?>
														<figure class="feat-medium">
															<a href="<?php the_permalink(); ?>"><?php if ( has_post_thumbnail() ) { the_post_thumbnail('homeland_property_medium'); } ?></a>
															<figcaption><a href="<?php the_permalink(); ?>"><i class="fa fa-link fa-lg"></i></a></figcaption>
														</figure><?php
													else : ?>
														<figure class="feat-thumb">
															<a href="<?php the_permalink(); ?>"><?php if ( has_post_thumbnail() ) { the_post_thumbnail(); } ?></a>
															<figcaption><a href="<?php the_permalink(); ?>"><i class="fa fa-link fa-lg"></i></a></figcaption>
															<?php
																$homeland_property_status = get_the_term_list ( $post->ID, 'homeland_property_status', ' ', ', ', '' );
																
																if(!empty( $homeland_property_status )) : ?>
																	<h4><?php echo $homeland_property_status; ?></h4><?php
																endif; 
															?>	
														</figure><?php
													endif;	
												endif;
											?>
											<div class="feat-desc">
												<?php the_title( '<h5><a href="' . get_permalink() . '">', '</a></h5>' );  ?>
												<span>
													<?php 
														echo esc_attr( get_post_meta($post->ID, 'homeland_area', true) ); echo "&nbsp;" . get_post_meta( $post->ID, 'homeland_area_unit', true );  echo ", "; 
														echo esc_attr( get_post_meta($post->ID, 'homeland_bedroom', true) ); echo "&nbsp;"; _e( 'Bedrooms', CODEEX_THEME_NAME ); echo ", "; 
														echo esc_attr( get_post_meta($post->ID, 'homeland_bathroom', true) ); echo "&nbsp;"; _e( 'Bathrooms', CODEEX_THEME_NAME ); echo ", "; 
														echo esc_attr( get_post_meta($post->ID, 'homeland_garage', true) ); echo "&nbsp;"; _e( 'Garage', CODEEX_THEME_NAME ); 
													?>
												</span>
												<?php
													if( !empty($homeland_price) ) : ?>
														<span class="price">
															<?php 
																echo esc_attr( get_option('homeland_property_currency') ); 
																echo number_format ( esc_attr ( $homeland_price ), 0, ".", "." ); 

																//if(!empty($homeland_price_per)) : echo "/" . $homeland_price_per; endif; 
																if(!empty($homeland_price_per)) : echo "/";
																	  _e($homeland_price_per , CODEEX_THEME_NAME );
																endif; 
															?>
														</span><?php
													endif;
												?>
											</div>
										</li><?php
									endwhile;
								?>							
							</ul>
						</div>
						<?php
					endif;
				?>
			</div>
		<?php	
	}


// Add term page
function lagomaggiore_taxonomy_add_new_meta_field() {
	// this will add the custom meta field to the add new term page
	?>
	<div class="form-field">
		<label for="term_meta[provincia]"><?php _e( 'Provincia', 'lagomaggiore' ); ?></label>
		<input type="text" name="term_meta[provincia]" id="term_meta[provincia]" value="">
		<p class="description"><?php _e( 'Inserisci la provincia','lagomaggiore' ); ?></p>
	</div>

	<div class="form-field">
		<label for="term_meta[regione]"><?php _e( 'Regione', 'lagomaggiore' ); ?></label>
		<input type="text" name="term_meta[regione]" id="term_meta[regione]" value="">
		<p class="description"><?php _e( 'Inserisci la regione','lagomaggiore' ); ?></p>
	</div>
<?php
}
add_action( 'homeland_property_location_add_form_fields', 'lagomaggiore_taxonomy_add_new_meta_field', 10, 2 );

// Edit term page
function lagomaggiore_taxonomy_edit_meta_field($term) {
 
	// put the term ID into a variable
	$t_id = $term->term_id;
 
	// retrieve the existing value(s) for this meta field. This returns an array
	$term_meta = get_option( "lagomaggiore_localita_$t_id" ); ?>
	<tr class="form-field">
	<th scope="row" valign="top"><label for="term_meta[provincia]"><?php _e( 'Provincia', 'lagomaggiore' ); ?></label></th>
		<td>
			<input type="text" name="term_meta[provincia]" id="term_meta[provincia]" value="<?php echo esc_attr( $term_meta['provincia'] ) ? esc_attr( $term_meta['provincia'] ) : ''; ?>">
			<p class="description"><?php _e( 'Inserisci la provincia','lagomaggiore' ); ?></p>
		</td>
	</tr>
	<tr class="form-field">
	<th scope="row" valign="top"><label for="term_meta[regione]"><?php _e( 'Regione', 'lagomaggiore' ); ?></label></th>
		<td>
			<input type="text" name="term_meta[regione]" id="term_meta[regione]" value="<?php echo esc_attr( $term_meta['regione'] ) ? esc_attr( $term_meta['regione'] ) : ''; ?>">
			<p class="description"><?php _e( 'Inserisci la regione','lagomaggiore' ); ?></p>
		</td>
	</tr>

<?php
}
add_action( 'homeland_property_location_edit_form_fields', 'lagomaggiore_taxonomy_edit_meta_field', 10, 2 );


// Save extra taxonomy fields callback function.
function lagomaggiore_save_taxonomy_custom_meta( $term_id ) {
	if ( isset( $_POST['term_meta'] ) ) {
		$t_id = $term_id;
		$term_meta = get_option( "lagomaggiore_localita_$t_id" );
		$cat_keys = array_keys( $_POST['term_meta'] );
		foreach ( $cat_keys as $key ) {
			if ( isset ( $_POST['term_meta'][$key] ) ) {
				$term_meta[$key] = $_POST['term_meta'][$key];
			}
		}
		// Save the option array.
		update_option( "lagomaggiore_localita_$t_id", $term_meta );
	}
}  
add_action( 'edited_homeland_property_location', 'lagomaggiore_save_taxonomy_custom_meta', 10, 2 );  
add_action( 'create_homeland_property_location', 'lagomaggiore_save_taxonomy_custom_meta', 10, 2 );


function print_pre($arr){
	if(LAGOMAGGIORE_DEBUG){
		echo '<pre>';
		if(is_array($arr)){
			print_r($arr);
		}
		else if(is_object($arr)){
			var_dump($arr);
		}
		else if(is_wp_error($arr )){
			echo 'wp_error..';
			echo $arr->get_error_message();
		}
		else if( false === $arr){
			echo "this value is false!";
		}
		else{
			echo "unknown..\n";
			echo $arr;
		}
		echo '</pre>';
	}
}


// add_filter('posts_where', 'lagomaggiore_advanced_search_query' );

// function lagomaggiore_advanced_search_query( $where )
// {
//   if( is_search() ) {

//     global $wpdb;
//     $query = get_search_query();
//     $query = like_escape( $query );
//     echo $query;
//     // include postmeta in search
//      $where .=" OR {$wpdb->posts}.ID IN (SELECT {$wpdb->postmeta}.post_id FROM {$wpdb->posts}, {$wpdb->postmeta} WHERE {$wpdb->postmeta}.meta_key = 'objectif' AND {$wpdb->postmeta}.meta_value LIKE '%$query%' AND {$wpdb->posts}.ID = {$wpdb->postmeta}.post_id)";

//      // include taxonomy in search
//     $where .=" OR {$wpdb->posts}.ID IN (SELECT {$wpdb->posts}.ID FROM {$wpdb->posts},{$wpdb->term_relationships},{$wpdb->terms} WHERE {$wpdb->posts}.ID = {$wpdb->term_relationships}.object_id AND {$wpdb->term_relationships}.term_taxonomy_id = {$wpdb->terms}.term_id AND {$wpdb->terms}.name LIKE '%$query%')";


//     if(WP_DEBUG)var_dump($where);
//     }
//     return $where;
// }


function lagomaggiore_get_localita_by_prov($regione = '', $provincia = '', $localita_slug = ''){
	global $wpdb;  // Bring our DB connection in scope
	$table_prefix = $wpdb->prefix; // Get the WP table prefix
	$table_name = "options"; // Assign the table name we wish to query
	$table = $table_prefix . $table_name; // Put it all together
	$queryresult = $wpdb->get_results("SELECT * FROM " . $table . " WHERE option_name LIKE 'lagomaggiore_localita_%' ", 0); 

	$ids = array();
	foreach ($queryresult as $result) {
		// estraggo la località dal termine corrente
		// se non corrisponde a quella passata a parametro, passo oltre
		$term_id = intval( str_replace( 'lagomaggiore_localita_', '', $result->option_name ) );
		$term = get_term( $term_id, 'homeland_property_location' );
		$buono = true;

		if( is_object( $term )){
			// se località non corrisponde, passo alla riga successiva
			if( !empty( $localita_slug ) && strtolower( $localita_slug ) != strtolower( $term->slug )){
				$buono = false;
			}
		}
		else{
			// se errore, passo alla riga successiva
			$buono = false;
		}

		// estraggo regione e provincia e faccio il controllo
		$valori = maybe_unserialize( $result->option_value );

		if( !empty( $regione ) &&  strtolower( $regione ) != strtolower( $valori['regione'] ) ):
			$buono = false;
		endif;

		if( !empty( $provincia ) && strtolower( $provincia ) != strtolower( $valori['provincia'] ) ){
			$buono = false;
		}

		if( $buono )
			array_push( $ids, $term_id );
	}
	return $ids;
}

add_filter( 'manage_edit-homeland_properties_columns', 'lagomaggiore_add_id_column', 99);

function lagomaggiore_add_id_column( $columns ) {
 	$column_meta = array( 'lagomaggiore_id' => __('Property ID', CODEEX_THEME_NAME) );
	$columns_new = array_slice($columns, 0, 1) + $column_meta + array_slice($columns, 1, NULL);
	return $columns_new;
}

// Add action to the manage post column to display the data
add_action( 'manage_homeland_properties_posts_custom_column' , 'lagomaggiore_custom_columns' );

/**
 * Display data in new columns
 *
 * @param  $column Current column
 *
 * @return Data for the column
 */
function lagomaggiore_custom_columns( $column ) {
	global $post;

	switch ( $column ) {
		case 'lagomaggiore_id':
			$metaData = get_post_meta( $post->ID, 'homeland_property_id', true );

			echo $metaData;
		break;
	}
}
