<?php
/*
	Template Name: Advance Search
*/
?>

<?php get_header(); ?>
	
	<?php lagomaggiore_homeland_advance_search(); ?>

	<!--BLOG LIST-->
	<section class="theme-pages">
		<div class="inside clear">

			<!--LEFT CONTAINER-->			
			<div class="left-container">	
				<?php lagomaggiore_homeland_property_sort_order(); ?>			
				<div class="agent-properties property-list clear">
					<?php
						$homeland_album_order = esc_attr( get_option('homeland_album_order') );
						$homeland_album_orderby = esc_attr( get_option('homeland_album_orderby') );
						$homeland_num_properties = esc_attr( get_option('homeland_num_properties') );
						$homeland_gmap_search = get_option('homeland_gmap_search');

						$homeland_filter_order = @$_GET['filter-order'];
						$homeland_filter_sort_by = @$_GET['filter-sort'];

						/*ORDER and SORT*/

						if(!empty($homeland_filter_order)) :
							$args_wp = array( 'post_type' => 'homeland_properties', 'orderby' => $homeland_filter_sort_by, 'order' => $homeland_filter_order, 'posts_per_page' => $homeland_num_properties, 'paged' => $paged );
						else :
							$args_wp = array( 'post_type' => 'homeland_properties', 'orderby' => $homeland_album_orderby, 'order' => $homeland_album_order, 'posts_per_page' => $homeland_num_properties, 'paged' => $paged );
						endif;

						if(!empty($homeland_filter_sort_by)) :
							$args_wp = array( 'post_type' => 'homeland_properties', 'orderby' => $homeland_filter_sort_by, 'order' => $homeland_filter_order, 'posts_per_page' => $homeland_num_properties, 'paged' => $paged, 'meta_key' => 'homeland_price' );
						else :
							$args_wp = array( 'post_type' => 'homeland_properties', 'orderby' => $homeland_album_orderby, 'order' => $homeland_album_order, 'posts_per_page' => $homeland_num_properties, 'paged' => $paged );
						endif;

						/*GOOGLE MAP*/

						// if(empty($homeland_gmap_search)) :
						// 	homeland_google_map_search();
				 	?>
					<!--section id="map-property"></section-->
					<?php
						// endif;

						// print_pre($args_wp);
						$args = apply_filters('homeland_advance_search_parameters', $args_wp);
						// print_pre($args);
						if( isset( $args['lagomaggiore_no_query'] ) ):
							_e( 'Dati incoerenti! Controllare', CODEEX_THEME_NAME );
						else:
              $wp_query = new WP_Query( $args );
							// print_pre($args);
              if ( $wp_query->have_posts() ) : ?>
              	<div class="grid cs-style-3">
              		<ul class="clear">
                	<?php
                     while ( $wp_query->have_posts() ) :
                    	$wp_query->the_post();
											get_template_part( 'loop', 'properties' );
                     endwhile;
                  ?>
                  </ul>
                	</div><?php
              else:
                 _e( 'Nessun risultato!', CODEEX_THEME_NAME );
              endif;
             endif;
					?>
				</div>
				<?php 
					if(esc_attr( get_option('homeland_pnav') )=="Next Previous Link") : 
						homeland_next_previous(); //modify function in "functions.php"...
					else : homeland_pagination(); //modify function in "functions.php"...
					endif; 
				?>
			</div>

			<!--SIDEBAR-->	
			<div class="sidebar"><?php get_sidebar(); ?></div>

		</div>

	</section>

<?php get_footer(); ?>