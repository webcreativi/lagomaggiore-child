<?php get_header(); ?>
	
	<?php lagomaggiore_homeland_advance_search(); //modify function in "includes/lib/custom-functions.php"... ?>

	<!--BLOG LIST-->
	<section class="theme-pages">

		<div class="inside clear">

			<!--LEFT CONTAINER-->			
			<div class="left-container">				
				<div class="blog-list clear">
					<?php
						if (have_posts()) : 
							while (have_posts()) : the_post(); 					
								get_template_part( 'loop', 'entry' );								
							endwhile;	
						else :
							_e( 'The Keyword you search cannot be found!', CODEEX_THEME_NAME );
						endif;					
					?>
				</div>
				<?php 
					if(esc_attr( get_option('homeland_pnav') )=="Next Previous Link") : 
						homeland_next_previous(); //modify function in "functions.php"...
					else : homeland_pagination(); //modify function in "functions.php"... 
					endif; 
				?>
			</div>

			<!--SIDEBAR-->	
			<div class="sidebar"><?php get_sidebar(); ?></div>

		</div>

	</section>

<?php get_footer(); ?>